use std::collections::{BTreeMap, HashMap};

use crate::utils::MultiLanguageItem;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, Hash)]
#[serde(untagged)]
pub enum Dependency {
    OneDependency(String),
    AlternativeDependency(Vec<String>),
}

#[derive(Serialize, Deserialize, Clone, Default, Debug, PartialEq, Eq)]
pub struct Permission {
    pub id: String,
    pub name: MultiLanguageItem,
    pub description: MultiLanguageItem,
    /// Other permissions this permission implies
    /// May also contain permissions of other apps
    pub includes: Vec<String>,
    /// Secrets (+ keys) accessible with this permission
    pub secrets: BTreeMap<String, Vec<String>>,
    /// Makes this permission "invisible" (Hidden from the UI) if requested by the apps listed in this field
    /// The * wildcard can be used to hide from all apps
    pub hidden: Vec<String>,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
pub enum SettingType {
    Enum,
    String,
    Bool,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct SettingsDeclaration {
    #[serde(rename = "type")]
    pub setting_type: SettingType,
    #[serde(default = "Vec::default")]
    #[serde(skip_serializing_if = "Vec::<String>::is_empty")]
    pub values: Vec<String>,
    pub name: BTreeMap<String, String>,
    pub description: BTreeMap<String, String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub default: Option<String>,
}

#[derive(Serialize, Deserialize, Clone, Default, Debug, PartialEq, Eq)]
pub struct Volume {
    pub minimum_size: u64,
    pub recommended_size: u64,
    pub name: MultiLanguageItem,
    pub description: MultiLanguageItem,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum Runtime {
    AppYml,
    Plugin(String),
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
#[serde(rename_all = "camelCase")]
pub struct OutputMetadata {
    /// The app id, only set in output
    pub id: String,
    /// The name of the app
    pub name: String,
    /// The version of the app
    pub version: semver::Version,
    /// The version of the app to display
    /// This is useful if the app is not using semver
    pub display_version: String,
    /// The category for the app
    pub category: BTreeMap<String, String>,
    /// A short tagline for the app
    pub tagline: BTreeMap<String, String>,
    // Developer name -> their website
    pub developers: BTreeMap<String, String>,
    /// A description of the app
    pub description: BTreeMap<String, String>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    /// Dependencies the app requires
    pub dependencies: Vec<Dependency>,
    /// Permissions the app has
    /// If a permission is from an app that is not listed in the dependencies, it is considered optional
    pub has_permissions: Vec<String>,
    /// Permissions this app exposes
    pub exposes_permissions: Vec<Permission>,
    /// App repository name -> repo URL
    pub repos: BTreeMap<String, String>,
    /// A support link for the app
    pub support: String,
    /// A list of promo images for the apps
    #[serde(default)]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub gallery: Vec<String>,
    /// The URL to the app icon
    pub icon: Option<String>,
    /// The path the "Open" link on the dashboard should lead to
    #[serde(skip_serializing_if = "Option::is_none")]
    pub path: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    /// The app's default username
    pub default_username: Option<String>,
    /// The app's default password.
    pub default_password: Option<String>,
    /// For "virtual" apps, the service the app implements
    #[serde(skip_serializing_if = "Option::is_none")]
    pub implements: Option<String>,
    #[serde(
        default,
        skip_serializing_if = "BTreeMap::<String, BTreeMap<String, String>>::is_empty"
    )]
    pub release_notes: BTreeMap<String, BTreeMap<String, String>>,
    /// The SPDX identifier of the app license
    pub license: String,
    /// Available settings for this app, directly copied from settings.json
    #[serde(
        default,
        skip_serializing_if = "BTreeMap::<String, SettingsDeclaration>::is_empty"
    )]
    pub settings: BTreeMap<String, SettingsDeclaration>,
    /// Whether this app should only allow one https domain
    pub single_https_domain: bool,
    /// Whether this app allows the user to change the domain
    pub allow_domain_change: bool,
    /// Volumes this app exposes
    pub volumes: HashMap<String, Volume>,
    /// Ports this app uses
    /// Before installing, app-manager clients need to check if any of these ports are already in use by other apps
    /// If so, the user needs to be notified
    pub ports: Vec<u16>,
    /// Exported data shared with plugins (Map plugin -> data)
    #[serde(skip_serializing_if = "Option::is_none")]
    pub exported_data: Option<HashMap<String, HashMap<String, serde_json::Value>>>,
    /// The runtime this app uses
    pub runtime: Runtime,
}
