use std::collections::{BTreeMap, HashMap};

use crate::generator::internal::permissions::get_app_perms;
use crate::generator::internal::ResolverCtx;
use itertools::Itertools;
use k8s_openapi::api::rbac::v1 as k8s_rbac;
use serde::{Deserialize, Serialize};
use slugify::slugify;

use crate::generator::metadata::types::{OutputMetadata, Runtime};
use crate::utils::{StringLike, StringOrNumber};

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum Runnable {
    Deployment(Box<Deployment>),
    Once(Box<Job>),
    OnAppUpdate(Box<Job>),
    Cron(Box<CronJob>),
}

impl Runnable {
    pub fn into_container(self) -> Container {
        match self {
            Runnable::Deployment(dep) => dep.container,
            Runnable::Once(job) => job.container,
            Runnable::OnAppUpdate(job) => job.container,
            Runnable::Cron(job) => job.container,
        }
    }

    pub fn get_container(&self) -> &Container {
        match self {
            Runnable::Deployment(dep) => &dep.container,
            Runnable::Once(job) => &job.container,
            Runnable::OnAppUpdate(job) => &job.container,
            Runnable::Cron(job) => &job.container,
        }
    }
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub enum Volume {
    Host(HostVolume),
    OwnLonghorn(LonghornVolume),
    Secret(SecretMount),
}

impl Volume {
    pub fn get_name(&self) -> &str {
        match self {
            Volume::Host(vol) => &vol.name,
            Volume::OwnLonghorn(vol) => &vol.name,
            Volume::Secret(vol) => &vol.name,
        }
    }

    pub fn get_slug(&self) -> String {
        match self {
            Volume::Host(vol) => slugify!(&vol.name),
            Volume::OwnLonghorn(vol) => slugify!(&vol.name),
            Volume::Secret(vol) => slugify!(&vol.name),
        }
    }

    pub fn get_sub_path(&self) -> Option<&String> {
        match self {
            Volume::Host(_) => None,
            Volume::OwnLonghorn(vol) => vol.sub_path.as_ref(),
            Volume::Secret(_) => None,
        }
    }

    pub fn get_mount_path(&self) -> &str {
        match self {
            Volume::Host(vol) => &vol.mount_path,
            Volume::OwnLonghorn(vol) => &vol.mount_path,
            Volume::Secret(vol) => &vol.mount_path,
        }
    }

    pub fn is_readonly(&self) -> bool {
        match self {
            Volume::Host(vol) => vol.is_readonly,
            Volume::OwnLonghorn(vol) => vol.is_readonly,
            Volume::Secret(_) => true,
        }
    }
}
#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct HostVolume {
    pub name: String,
    pub host_path: String,
    pub mount_path: String,
    pub is_readonly: bool,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct LonghornVolume {
    pub name: String,
    pub mount_path: String,
    pub sub_path: Option<String>,
    pub is_readonly: bool,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct SecretMount {
    pub name: String,
    pub mount_path: String,
}

#[derive(Clone, PartialEq, Debug, Default, Serialize, Deserialize)]
pub struct SecretRef {
    pub secret: String,
    pub key: String,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub enum EnvVar {
    StringLike(StringLike),
    SecretRef(SecretRef),
}

#[derive(Clone, PartialEq, Debug, Default, Serialize, Deserialize)]
pub struct Ports {
    pub udp: HashMap<u16, u16>,
    pub tcp: HashMap<u16, u16>,
}

impl Ports {
    pub fn keys(&self) -> Vec<u16> {
        return self
            .tcp
            .keys()
            .copied()
            .chain(self.udp.clone().keys().copied())
            .sorted()
            .dedup()
            .collect();
    }

    pub fn values(&self) -> Vec<u16> {
        return self
            .tcp
            .values()
            .copied()
            .chain(self.udp.clone().values().copied())
            .sorted()
            .dedup()
            .collect();
    }

    pub fn is_empty(&self) -> bool {
        self.tcp.is_empty() && self.udp.is_empty()
    }

    pub fn append(&mut self, other: &Ports) {
        self.tcp.extend(other.tcp.clone());
        self.udp.extend(other.udp.clone());
    }
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct Service {
    pub target_container: String,
    pub name: String,
    pub r#type: ServiceType,
    pub ports: Ports,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub enum ServiceType {
    LoadBalancer,
    ClusterIp,
}

#[derive(Clone, PartialEq, Debug, Default, Serialize, Deserialize)]
pub struct Container {
    pub cap_add: Vec<String>,
    pub command: Option<Vec<String>>,
    pub entrypoint: Option<Vec<String>>,
    pub environment: BTreeMap<String, EnvVar>,
    pub hostname: Option<String>,
    pub image: String,
    pub privileged: bool,
    pub restart: Option<String>,
    pub stop_grace_period: Option<String>,
    pub uid: Option<u32>,
    pub gid: Option<u32>,
    pub volumes: Vec<Volume>,
    pub working_dir: Option<String>,
    pub shm_size: Option<StringOrNumber>,
    pub ulimits: Option<serde_json::Value>,
    pub host_network: bool,
    /// Ports this container exposes to other containers
    pub exposes: Ports,
    pub fs_group: Option<u32>,
    pub service_account: Option<ServiceAccount>,
}

#[derive(Clone, PartialEq, Debug, Default, Serialize, Deserialize)]
pub struct CronJob {
    pub container: Container,
    /// The cron schedule at which to execute this service (only if type is CronJob)
    pub schedule: String,
}

#[derive(Clone, PartialEq, Debug, Default, Serialize, Deserialize)]
pub struct Job {
    pub container: Container,
}

#[derive(Clone, PartialEq, Debug, Default, Serialize, Deserialize)]
pub struct Deployment {
    pub container: Container,
}

#[derive(Clone, PartialEq, Debug, Default, Serialize, Deserialize)]
pub struct ServiceAccount {
    pub cluster_rules: Vec<k8s_rbac::PolicyRule>,
    pub ns_rules: HashMap<String, Vec<k8s_rbac::PolicyRule>>,
}

#[derive(Clone, PartialEq, Debug, Default, Serialize, Deserialize)]
pub struct Secret {
    pub name: String,
    pub data: BTreeMap<String, Box<[u8]>>,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum PluginType {
    Context,
    Source,
    Runtime,
    CustomResource,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct PluginInfo {
    pub name: String,
    pub r#type: PluginType,
    pub endpoint: String,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct CustomResource {
    pub plugin: String,
    pub definition: serde_json::Value,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct InternalAppRepresentation {
    pub(in crate::generator) metadata: OutputMetadata,
    pub(in crate::generator) containers: BTreeMap<String, Runnable>,
    pub(in crate::generator) services: Vec<Service>,
    pub(in crate::generator) ingress: Vec<Ingress>,
    pub(in crate::generator) plugins: Vec<PluginInfo>,
    pub(in crate::generator) secrets: Vec<Secret>,
    pub(in crate::generator) custom_resources: Vec<CustomResource>,
}

#[derive(Clone, PartialEq, Debug, Default, Serialize, Deserialize)]
pub struct Ingress {
    pub target_service: String,
    pub target_app: Option<String>,
    pub path_prefix: Option<String>,
    pub target_port: u16,
    pub enable_compression: bool,
    pub strip_prefix: bool,
}

impl InternalAppRepresentation {
    pub async fn new(
        metadata: OutputMetadata,
        containers: BTreeMap<String, Runnable>,
        services: Vec<Service>,
        ingress: Vec<Ingress>,
        secrets: Vec<Secret>,
        plugins: Vec<PluginInfo>,
        custom_resources: Vec<CustomResource>,
        ctx: &mut ResolverCtx,
    ) -> Self {
        let mut tmp = Self {
            metadata,
            containers,
            services,
            ingress,
            plugins,
            secrets,
            custom_resources,
        };
        let mut additional_perms = tmp.permissions(ctx).await;
        tmp.metadata.has_permissions.append(&mut additional_perms);
        tmp
    }

    async fn permissions(&self, ctx: &mut ResolverCtx) -> Vec<String> {
        ctx.insert(
            self.metadata.id.clone(),
            self.metadata.exposes_permissions.clone(),
        );
        get_app_perms(self, ctx).await
    }

    pub fn get_public_ports(&self) -> Vec<u16> {
        let mut ports = Vec::new();
        for service in &self.services {
            if service.r#type == ServiceType::LoadBalancer {
                let mut new_ports = service.ports.keys();
                ports.append(&mut new_ports)
            }
        }
        ports.sort();
        ports.dedup();
        ports
    }

    pub fn get_ingress(&self) -> &Vec<Ingress> {
        &self.ingress
    }

    pub fn into_ingress(self) -> Vec<Ingress> {
        self.ingress
    }

    pub fn get_metadata(&self) -> &OutputMetadata {
        &self.metadata
    }

    pub fn into_metadata(self) -> OutputMetadata {
        self.metadata
    }

    pub fn get_plugins(&self) -> &[PluginInfo] {
        &self.plugins
    }

    pub fn into_plugins(self) -> Vec<PluginInfo> {
        self.plugins
    }

    pub fn set_runtime(&mut self, runtime: Runtime) {
        self.metadata.runtime = runtime;
    }
}
