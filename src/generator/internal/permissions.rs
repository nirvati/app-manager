mod resolver;

pub use resolver::ResolverCtx;

use crate::generator::internal::types::{InternalAppRepresentation, PluginType, Volume};

use async_recursion::async_recursion;

#[async_recursion]
pub async fn get_app_perms(app: &InternalAppRepresentation, ctx: &mut ResolverCtx) -> Vec<String> {
    let mut app_perms = vec![];

    for runnable in app.containers.values() {
        let container = runnable.get_container();
        for vol in &container.volumes {
            match vol {
                Volume::Host(_) => {
                    app_perms.push("builtin/root".to_string());
                }
                Volume::OwnLonghorn(_) => {}
                Volume::Secret(_) => {
                }
            };
        }

        if container.host_network {
            app_perms.push("builtin/network".to_string());
        }

        for capability in &container.cap_add {
            match capability.as_str() {
                "CAP_NET_RAW" => {
                    app_perms.push("builtin/network".to_string());
                }
                _ => {
                    app_perms.push("builtin/root".to_string());
                }
            }
        }

        if container.privileged {
            app_perms.push("builtin/root".to_string());
        }

        // TODO: Make this more fine-grained
        if container.service_account.is_some() {
            app_perms.push("builtin/root".to_string());
        }
    }

    if app
        .plugins
        .iter()
        .any(|plugin| plugin.r#type == PluginType::Source)
    {
        app_perms.push("builtin/plugins/source".to_string());
    }
    if app
        .plugins
        .iter()
        .any(|plugin| plugin.r#type == PluginType::Context)
    {
        app_perms.push("builtin/plugins/context".to_string());
    }
    if app
        .plugins
        .iter()
        .any(|plugin| plugin.r#type == PluginType::Runtime)
    {
        app_perms.push("builtin/plugins/runtime".to_string());
    }
    if app
        .plugins
        .iter()
        .any(|plugin| plugin.r#type == PluginType::CustomResource)
    {
        app_perms.push("builtin/plugins/custom-resource".to_string());
    }
    app_perms.dedup();
    app_perms
}
