use crate::generator::metadata::types::Permission;
use crate::instance_type::InstanceType;
use crate::parser::load_app;
use crate::plugins::api::UserState;
use anyhow::Result;
use std::collections::BTreeMap;
use std::path::PathBuf;

#[derive(Clone)]
pub struct ResolverCtx {
    processed_apps: BTreeMap<String, Vec<Permission>>,
    apps_root: PathBuf,
    user_state: UserState,
    nirvati_seed: String,
    instance_type: InstanceType,
}

impl ResolverCtx {
    pub fn get(&self, app: &str) -> Option<&Vec<Permission>> {
        self.processed_apps.get(app)
    }

    pub async fn get_or_load(&mut self, app: &str) -> Result<&Vec<Permission>> {
        if !self.processed_apps.contains_key(app) {
            let apps_root = self.apps_root.clone();
            let nirvati_seed = self.nirvati_seed.clone();
            let parsed = load_app(
                &apps_root,
                app,
                self.user_state.clone(),
                &nirvati_seed,
                self.instance_type,
                None,
                self,
            )
            .await?
            .into_metadata();
            self.processed_apps
                .insert(app.to_string(), parsed.exposes_permissions);
        }
        Ok(self.processed_apps.get(app).unwrap())
    }

    pub fn insert(&mut self, app: String, permissions: Vec<Permission>) {
        self.processed_apps.insert(app, permissions);
    }

    pub fn new(
        apps_root: PathBuf,
        user_state: UserState,
        nirvati_seed: String,
        instance_type: InstanceType,
    ) -> Self {
        Self {
            processed_apps: BTreeMap::new(),
            apps_root,
            user_state,
            nirvati_seed,
            instance_type,
        }
    }
}
