use anyhow::Result;
use k8s_openapi::api::apps::v1 as k8s_apps;
use k8s_openapi::api::batch::v1 as k8s_batch;
use k8s_openapi::api::core::v1 as k8s;
use std::path::Path;

use crate::generator::internal::types::{InternalAppRepresentation, Runnable};
use crate::generator::kubernetes::generator::deployment::generate_deployment;
use crate::generator::kubernetes::generator::pvc::generate_pvc;
use crate::generator::kubernetes::generator::secret::generate_secrets;
use crate::plugins::api::UserState;

mod cronjob;
mod deployment;
pub mod ingress;
mod job;
mod middleware;
mod pod;
mod pvc;
mod secret;
mod service_account;
mod services;
mod utils;
mod volume;

#[derive(Debug, Default, Clone)]
pub struct KubernetesConfig {
    pub deployments: Vec<k8s_apps::Deployment>,
    pub services: Vec<k8s::Service>,
    pub cronjobs: Vec<k8s_batch::CronJob>,
    pub jobs: Vec<k8s_batch::Job>,
    pub pvcs: Vec<k8s::PersistentVolumeClaim>,
    pub service_accounts: Vec<service_account::ServiceAccount>,
    pub middlewares: Vec<k8s_crds_traefik::middlewares::Middleware>,
    pub secrets: Vec<k8s::Secret>,
    pub others: Vec<serde_yaml::Value>,
}

pub async fn generate_kubernetes_config(
    app: InternalAppRepresentation,
    user_state: UserState,
    apps_root: &Path,
) -> Result<KubernetesConfig> {
    let mut deployments = Vec::new();
    let mut jobs = Vec::new();
    let mut cronjobs = Vec::new();
    let services = app.services.iter().map(|svc| svc.into()).collect();
    let mut service_accounts = Vec::new();
    let app_id = app.metadata.id.clone();
    for (name, runnable) in app.containers.clone() {
        if let Some(service_account) = service_account::generate_service_account(
            &app_id,
            &user_state.user,
            name.clone(),
            runnable.get_container(),
        ) {
            service_accounts.push(service_account);
        }
        match runnable {
            Runnable::Deployment(deployment) => {
                deployments.push(generate_deployment(name.clone(), *deployment));
            }
            Runnable::Once(job) => {
                jobs.push(job::generate_job_onetime(
                    name.clone(),
                    job.container.clone(),
                ));
            }
            Runnable::OnAppUpdate(job) => {
                jobs.push(job::generate_job_on_update(
                    &app.metadata.version.to_string(),
                    name.clone(),
                    job.container.clone(),
                ));
            }
            Runnable::Cron(cronjob) => {
                cronjobs.push(cronjob::generate_cronjob(name.clone(), *cronjob.clone()));
            }
        }
    }
    Ok(KubernetesConfig {
        deployments,
        services,
        cronjobs,
        jobs,
        pvcs: app
            .metadata
            .volumes
            .iter()
            .map(|(vol_name, vol)| generate_pvc(vol_name, vol))
            .collect(),
        service_accounts,
        middlewares: app
            .ingress
            .iter()
            .filter_map(|ingress| {
                if ingress.strip_prefix
                    && ingress
                        .path_prefix
                        .as_ref()
                        .is_some_and(|prefix| prefix != "/")
                {
                    Some(middleware::strip_prefix_mw(
                        ingress.path_prefix.as_ref().unwrap(),
                    ))
                } else {
                    None
                }
            })
            .collect(),
        secrets: generate_secrets(&app.secrets),
        others: crate::plugins::convert_crs(apps_root, &app_id, &app.custom_resources, user_state)
            .await?,
    })
}
