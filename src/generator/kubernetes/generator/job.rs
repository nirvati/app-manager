use k8s_openapi::api::batch::v1 as k8s_batch;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use slugify::slugify;

use crate::generator::internal::types as internal;
use crate::generator::internal::types::{Job, Runnable};
use crate::generator::kubernetes::generator::pod::generate_pod;

pub fn generate_job_onetime(service_name: String, service: internal::Container) -> k8s_batch::Job {
    k8s_batch::Job {
        spec: Some(k8s_batch::JobSpec {
            template: generate_pod(
                service_name.clone(),
                Runnable::Once(Box::new(Job { container: service })),
            ),
            ..Default::default()
        }),
        metadata: k8s_meta::ObjectMeta {
            name: Some(service_name),
            ..Default::default()
        },
        ..Default::default()
    }
}

pub fn generate_job_on_update(
    app_version: &str,
    service_name: String,
    service: internal::Container,
) -> k8s_batch::Job {
    k8s_batch::Job {
        spec: Some(k8s_batch::JobSpec {
            template: generate_pod(
                service_name.clone(),
                Runnable::OnAppUpdate(Box::new(Job { container: service })),
            ),
            ..Default::default()
        }),
        metadata: k8s_meta::ObjectMeta {
            name: Some(format!("{}-{}", service_name, slugify!(app_version))),
            ..Default::default()
        },
        ..Default::default()
    }
}
