use itertools::Itertools;
use slugify::slugify;

use crate::generator::internal::types::Volume;

pub fn generate_volume(vol: Volume) -> k8s_openapi::api::core::v1::Volume {
    k8s_openapi::api::core::v1::Volume {
        name: vol.get_slug(),
        persistent_volume_claim: if let Volume::OwnLonghorn(ref vol) = vol {
            Some(
                k8s_openapi::api::core::v1::PersistentVolumeClaimVolumeSource {
                    claim_name: slugify!(&vol.name),
                    read_only: Some(vol.is_readonly),
                },
            )
        } else {
            None
        },
        host_path: if let Volume::Host(ref vol) = vol {
            Some(k8s_openapi::api::core::v1::HostPathVolumeSource {
                path: vol.host_path.clone(),
                ..Default::default()
            })
        } else {
            None
        },
        secret: if let Volume::Secret(ref vol) = vol {
            Some(k8s_openapi::api::core::v1::SecretVolumeSource {
                secret_name: Some(vol.name.clone()),
                ..Default::default()
            })
        } else {
            None
        },
        ..Default::default()
    }
}

pub fn generate_volumes(volumes: Vec<Volume>) -> Vec<k8s_openapi::api::core::v1::Volume> {
    volumes.into_iter().map(generate_volume).dedup().collect()
}
