use std::collections::BTreeMap;

use k8s_openapi::api::core::v1 as k8s;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use slugify::slugify;

use crate::generator::internal::types::{EnvVar, Runnable};

use super::utils;
use super::volume::generate_volumes;

pub fn generate_pod(service_name: String, service: Runnable) -> k8s::PodTemplateSpec {
    let container = service.into_container();
    k8s::PodTemplateSpec {
        metadata: Some(k8s_meta::ObjectMeta {
            labels: Some(BTreeMap::from([(
                "container".to_owned(),
                service_name.clone(),
            )])),
            ..Default::default()
        }),
        spec: Some(k8s::PodSpec {
            service_account_name: container.service_account.map(|_| service_name.clone()),
            security_context: Some(k8s::PodSecurityContext {
                run_as_user: container.uid.map(|uid| uid as i64),
                run_as_group: container.gid.map(|gid| gid as i64),
                fs_group: container
                    .fs_group
                    .map(|gid| gid as i64)
                    .or(container.gid.map(|gid| gid as i64)),
                ..Default::default()
            }),
            termination_grace_period_seconds: container.stop_grace_period.map(|s| {
                // String can be a time duration, we need to parse it as such
                let duration = parse_duration::parse(&s).unwrap();
                duration.as_secs() as i64
            }),
            hostname: container.hostname,
            host_network: container.host_network.then_some(true),
            volumes: Some(generate_volumes(container.volumes.clone())),
            restart_policy: container
                .restart
                .and_then(|policy| utils::compose_restart_policy_to_kubernetes(&policy).ok()),
            containers: vec![k8s::Container {
                name: service_name,
                image: Some(container.image),
                // This is correct, kubernetes names things better
                args: Some(container.command.unwrap_or_default()),
                command: Some(container.entrypoint.unwrap_or_default()),
                env: Some(
                    container
                        .environment
                        .into_iter()
                        .map(|(k, v)| k8s::EnvVar {
                            name: k,
                            value: if let EnvVar::StringLike(v) = &v {
                                Some(v.clone().into())
                            } else {
                                None
                            },
                            value_from: if let EnvVar::SecretRef(v) = v {
                                Some(k8s::EnvVarSource {
                                    secret_key_ref: Some(k8s::SecretKeySelector {
                                        name: Some(v.secret),
                                        key: v.key,
                                        ..Default::default()
                                    }),
                                    ..Default::default()
                                })
                            } else {
                                None
                            },
                        })
                        .collect(),
                ),
                volume_mounts: Some(
                    container
                        .volumes
                        .into_iter()
                        .map(|vol| k8s::VolumeMount {
                            name: slugify!(&vol.get_slug()),
                            mount_path: vol.get_mount_path().to_owned(),
                            sub_path: vol.get_sub_path().cloned(),
                            ..Default::default()
                        })
                        .collect(),
                ),
                ports: Some(
                    container
                        .exposes
                        .tcp
                        .into_values()
                        .map(|container_port| k8s::ContainerPort {
                            container_port: container_port as i32,
                            ..Default::default()
                        })
                        .chain(container.exposes.udp.into_values().map(|container_port| {
                            k8s::ContainerPort {
                                container_port: container_port as i32,
                                protocol: Some("UDP".to_string()),
                                ..Default::default()
                            }
                        }))
                        .collect(),
                ),
                security_context: Some(k8s::SecurityContext {
                    privileged: Some(container.privileged),
                    capabilities: Some(k8s::Capabilities {
                        add: Some(container.cap_add),
                        ..Default::default()
                    }),
                    ..Default::default()
                }),
                ..Default::default()
            }],
            ..Default::default()
        }),
    }
}
