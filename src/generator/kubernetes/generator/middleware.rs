use k8s_crds_traefik::{Middleware, MiddlewareSpec, MiddlewareStripPrefix};
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use slugify::slugify;

pub fn strip_prefix_mw(prefix: &str) -> Middleware {
    Middleware {
        metadata: k8s_meta::ObjectMeta {
            name: Some(format!("strip-{}-prefix", slugify!(prefix))),
            ..Default::default()
        },
        spec: MiddlewareSpec {
            strip_prefix: Some(MiddlewareStripPrefix {
                prefixes: Some(vec![prefix.to_string()]),
            }),
            ..Default::default()
        },
    }
}
