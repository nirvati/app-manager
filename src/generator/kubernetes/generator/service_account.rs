use k8s_openapi::api::core::v1 as k8s;
use k8s_openapi::api::rbac::v1 as k8s_rbac;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;

use crate::generator::internal::types::Container;

#[derive(Debug, Default, Clone)]
pub struct ServiceAccount {
    pub cluster_role: k8s_rbac::ClusterRole,
    pub cluster_role_binding: k8s_rbac::ClusterRoleBinding,
    pub account: k8s::ServiceAccount,
}

pub fn generate_service_account(
    app_id: &str,
    user: &str,
    service_name: String,
    service: &Container,
) -> Option<ServiceAccount> {
    service
        .service_account
        .clone()
        .map(|service_account| ServiceAccount {
            cluster_role: k8s_rbac::ClusterRole {
                metadata: k8s_meta::ObjectMeta {
                    name: Some(format!("nirvati:{}:{}:{}", user, app_id, service_name)),
                    ..Default::default()
                },
                rules: Some(service_account.cluster_rules),
                ..Default::default()
            },
            cluster_role_binding: k8s_rbac::ClusterRoleBinding {
                metadata: k8s_meta::ObjectMeta {
                    name: Some(format!("nirvati:{}:{}:{}", user, app_id, service_name)),
                    ..Default::default()
                },
                role_ref: k8s_rbac::RoleRef {
                    api_group: "rbac.authorization.k8s.io".to_string(),
                    kind: "ClusterRole".to_string(),
                    name: format!("nirvati:{}:{}:{}", user, app_id, service_name),
                },
                subjects: Some(vec![k8s_rbac::Subject {
                    kind: "ServiceAccount".to_string(),
                    name: service_name.clone(),
                    namespace: Some(format!("{}-{}", user, app_id)),
                    ..Default::default()
                }]),
            },
            account: k8s::ServiceAccount {
                metadata: k8s_meta::ObjectMeta {
                    name: Some(service_name.clone()),
                    ..Default::default()
                },
                ..Default::default()
            },
        })
}
