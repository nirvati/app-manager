use k8s_openapi::api::batch::v1 as k8s_batch;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;

use crate::generator::internal::types as internal;
use crate::generator::internal::types::Runnable;
use crate::generator::kubernetes::generator::pod::generate_pod;

pub fn generate_cronjob(service_name: String, service: internal::CronJob) -> k8s_batch::CronJob {
    k8s_batch::CronJob {
        spec: Some(k8s_batch::CronJobSpec {
            schedule: service.schedule.clone(),
            job_template: k8s_batch::JobTemplateSpec {
                spec: Some(k8s_batch::JobSpec {
                    template: generate_pod(
                        service_name.clone(),
                        Runnable::Cron(Box::from(service)),
                    ),
                    ..Default::default()
                }),
                ..Default::default()
            },
            ..Default::default()
        }),
        metadata: k8s_meta::ObjectMeta {
            name: Some(service_name),
            ..Default::default()
        },
        ..Default::default()
    }
}
