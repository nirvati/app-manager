use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use k8s_openapi::apimachinery::pkg::util::intstr::IntOrString;
use slugify::slugify;

use crate::generator::internal::types::Ingress;

pub fn get_app_ingress(
    domain: Option<String>,
    sources: &[Ingress],
    entrypoints: Vec<String>,
    is_tls: bool,
    user: &str,
) -> k8s_crds_traefik::IngressRoute {
    k8s_crds_traefik::IngressRoute {
        metadata: k8s_meta::ObjectMeta {
            name: if let Some(domain) = domain.as_ref() {
                Some(slugify!(domain))
            } else {
                Some("ingress".to_string())
            },
            ..Default::default()
        },
        spec: k8s_crds_traefik::IngressRouteSpec {
            entry_points: Some(entrypoints),
            routes: sources
                .iter()
                .map(|source| {
                    let mut middlewares = Vec::new();
                    if source.enable_compression {
                        middlewares.push(k8s_crds_traefik::IngressRouteRoutesMiddlewares {
                            name: "compress".to_owned(),
                            namespace: Some("nirvati".to_string()),
                        })
                    }
                    if source.strip_prefix
                        && source
                            .path_prefix
                            .as_ref()
                            .is_some_and(|prefix| prefix != "/")
                    {
                        middlewares.push(k8s_crds_traefik::IngressRouteRoutesMiddlewares {
                            name: format!(
                                "strip-{}-prefix",
                                slugify!(source.path_prefix.as_ref().unwrap())
                            ),
                            namespace: None,
                        })
                    }
                    k8s_crds_traefik::IngressRouteRoutes {
                        kind: k8s_crds_traefik::IngressRouteRoutesKind::Rule,
                        r#match: if let Some(domain) = domain.as_ref() {
                            format!(
                                "PathPrefix(`{}`) && Host(`{}`)",
                                source.path_prefix.clone().unwrap_or("/".to_string()),
                                domain
                            )
                        } else {
                            format!(
                                "PathPrefix(`{}`)",
                                source.path_prefix.clone().unwrap_or("/".to_string())
                            )
                        },
                        middlewares: if middlewares.is_empty() {
                            None
                        } else {
                            Some(middlewares)
                        },
                        services: Some(vec![k8s_crds_traefik::IngressRouteRoutesServices {
                            name: source.target_service.clone(),
                            port: Some(IntOrString::Int(source.target_port as i32)),
                            namespace: source
                                .target_app
                                .as_ref()
                                .map(|app| format!("{}-{}", user, app)),
                            ..Default::default()
                        }]),
                        priority: None,
                    }
                })
                .collect(),

            tls: if is_tls && domain.is_some() {
                Some(k8s_crds_traefik::IngressRouteTls {
                    secret_name: Some(format!("{}-tls", slugify!(&domain.as_ref().unwrap()))),
                    ..Default::default()
                })
            } else {
                None
            },
        },
    }
}

// Returns an ingress route for the given app, as well as one that implements a redirect from http to https.
pub fn get_ingress_routes(
    domain: String,
    sources: &[Ingress],
    user: &str,
) -> [k8s_crds_traefik::IngressRoute; 2] {
    [
        get_app_ingress(
            Some(domain.clone()),
            sources,
            vec!["websecure".to_string()],
            true,
            user,
        ),
        k8s_crds_traefik::IngressRoute {
            metadata: k8s_meta::ObjectMeta {
                name: Some(format!("{}-https-redirect", slugify!(&domain))),
                ..Default::default()
            },
            spec: k8s_crds_traefik::IngressRouteSpec {
                entry_points: Some(vec!["web".to_owned()]),
                routes: vec![k8s_crds_traefik::IngressRouteRoutes {
                    kind: k8s_crds_traefik::IngressRouteRoutesKind::Rule,
                    r#match: format!("Host(`{}`)", domain),
                    priority: None,
                    middlewares: Some(vec![k8s_crds_traefik::IngressRouteRoutesMiddlewares {
                        name: "https-redirect".to_owned(),
                        namespace: Some("nirvati".to_string()),
                    }]),
                    services: None,
                }],

                tls: None,
            },
        },
    ]
}
