use std::collections::BTreeMap;

use k8s_openapi::api::core::v1 as k8s;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use k8s_openapi::apimachinery::pkg::util::intstr::IntOrString;

use crate::generator::internal::types::{Service, ServiceType};

impl From<&Service> for k8s::Service {
    fn from(service: &Service) -> Self {
        k8s::Service {
            metadata: k8s_meta::ObjectMeta {
                name: Some(service.name.clone()),
                ..Default::default()
            },
            spec: Some(k8s::ServiceSpec {
                selector: Some(BTreeMap::from([(
                    "container".to_owned(),
                    service.name.clone(),
                )])),
                type_: Some(match service.r#type {
                    ServiceType::LoadBalancer => "LoadBalancer".to_string(),
                    ServiceType::ClusterIp => "ClusterIP".to_string(),
                }),
                ports: Some(
                    service
                        .ports
                        .tcp
                        .iter()
                        .map(|(public_port, internal_port)| k8s::ServicePort {
                            port: *public_port as i32,
                            target_port: Some(IntOrString::Int(*internal_port as i32)),
                            name: Some(format!("{}-{}", service.name, public_port)),
                            ..Default::default()
                        })
                        .chain(
                            service
                                .ports
                                .udp
                                .iter()
                                .map(|(public_port, internal_port)| k8s::ServicePort {
                                    port: *public_port as i32,
                                    target_port: Some(IntOrString::Int(*internal_port as i32)),
                                    name: Some(format!("{}-{}", service.name, public_port)),
                                    protocol: Some("UDP".to_string()),
                                    ..Default::default()
                                }),
                        )
                        .collect(),
                ),
                ..Default::default()
            }),
            status: None,
        }
    }
}
