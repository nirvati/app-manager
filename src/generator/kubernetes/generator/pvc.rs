use std::collections::BTreeMap;

use k8s_openapi::api::core::v1 as k8s;
use k8s_openapi::apimachinery::pkg::api::resource::Quantity;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;

use crate::generator::metadata::types::Volume;

pub fn generate_pvc(volume_name: &str, volume: &Volume) -> k8s::PersistentVolumeClaim {
    k8s::PersistentVolumeClaim {
        metadata: k8s_meta::ObjectMeta {
            name: Some(volume_name.to_string()),
            ..Default::default()
        },
        spec: Some(k8s::PersistentVolumeClaimSpec {
            access_modes: Some(vec!["ReadWriteMany".to_string()]),
            storage_class_name: Some("longhorn".to_string()),
            resources: Some(k8s::VolumeResourceRequirements {
                requests: Some(BTreeMap::from([(
                    "storage".to_string(),
                    Quantity(volume.recommended_size.clone().to_string()),
                )])),
                ..Default::default()
            }),
            ..Default::default()
        }),
        status: None,
    }
}
