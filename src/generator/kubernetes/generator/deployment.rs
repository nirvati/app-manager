use std::collections::BTreeMap;

use k8s_openapi::api::apps::v1 as k8s_apps;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;

use crate::generator::internal::types::{Deployment, Runnable};
use crate::generator::kubernetes::generator::pod::generate_pod;

pub fn generate_deployment(service_name: String, service: Deployment) -> k8s_apps::Deployment {
    k8s_apps::Deployment {
        spec: Some(k8s_apps::DeploymentSpec {
            selector: k8s_meta::LabelSelector {
                match_labels: Some(BTreeMap::from([(
                    "container".to_owned(),
                    service_name.clone(),
                )])),
                ..Default::default()
            },
            template: generate_pod(
                service_name.clone(),
                Runnable::Deployment(Box::from(service)),
            ),
            ..Default::default()
        }),
        metadata: k8s_meta::ObjectMeta {
            name: Some(service_name),
            ..Default::default()
        },
        ..Default::default()
    }
}
