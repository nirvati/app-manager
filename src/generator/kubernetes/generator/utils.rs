use anyhow::{bail, Result};

pub fn compose_restart_policy_to_kubernetes(policy: &str) -> Result<String> {
    match policy.to_lowercase().as_str() {
        "no" => Ok("Never".to_string()),
        "never" => Ok("Never".to_string()),
        "always" => Ok("Always".to_string()),
        "on-failure" => Ok("OnFailure".to_string()),
        _ => bail!("Unknown restart policy: {}", policy),
    }
}
