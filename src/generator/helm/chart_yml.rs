use crate::generator::helm::types::{Chart, Maintainer};
use crate::generator::metadata::types::OutputMetadata;

pub fn generate_chart_yml(metadata: OutputMetadata) -> Chart {
    let description = metadata
        .description
        .get("en")
        .unwrap_or(
            metadata
                .description
                .values()
                .next()
                .unwrap_or(&"".to_string()),
        )
        .clone();
    let category = metadata
        .category
        .get("en")
        .unwrap_or(metadata.category.values().next().unwrap_or(&"".to_string()))
        .clone();
    Chart {
        api_version: "v2".to_string(),
        name: metadata.id,
        version: metadata.version.to_string(),
        kube_version: None,
        description: Some(description),
        chart_type: Some("application".to_string()), // TODO: Check if we should implement library charts in the future
        keywords: Some(vec![category]),
        home: metadata.developers.values().next().cloned(),
        sources: Some(metadata.repos.values().cloned().collect()),
        dependencies: None,
        maintainers: Some(
            metadata
                .developers
                .iter()
                .map(|(dev, website)| Maintainer {
                    name: dev.clone(),
                    email: None,
                    url: Some(website.clone()),
                })
                .collect(),
        ),
        icon: metadata.icon,
        app_version: Some(metadata.display_version),
        deprecated: None,
        annotations: None,
    }
}
