use std::io::Write;

use anyhow::Result;
use flate2::write::GzEncoder;
use tar::Header;

use crate::generator::kubernetes::generator::KubernetesConfig;
use crate::generator::metadata::types::OutputMetadata;

mod chart_yml;
pub mod types;

fn write_file<T: Write>(archive: &mut tar::Builder<T>, path: &str, data: &[u8]) -> Result<()> {
    let mut header = Header::new_gnu();
    header.set_size(data.len() as u64);
    header.set_cksum();
    archive.append_data(&mut header, path, data)?;
    Ok(())
}

fn multidoc_serialize<T: serde::Serialize>(data: Vec<T>) -> Result<String> {
    let mut output = String::new();
    for item in data {
        output.push_str("---\n");
        output.push_str(&serde_yaml::to_string(&item)?);
        output.push('\n');
    }
    Ok(output)
}

pub fn generate_chart(
    app: KubernetesConfig,
    metadata: OutputMetadata,
    user: &str,
) -> Result<Vec<u8>> {
    let output = Vec::new();
    let gz_encoder = GzEncoder::new(output, flate2::Compression::default());
    let mut tar = tar::Builder::new(gz_encoder);
    let app_id = format!("{}-{}", user, metadata.id.clone());
    let chart = chart_yml::generate_chart_yml(metadata);
    let data = serde_yaml::to_string(&chart)?;
    write_file(
        &mut tar,
        format!("{}/Chart.yaml", &app_id).as_str(),
        data.as_bytes(),
    )?;
    let data = multidoc_serialize(app.deployments)?;
    write_file(
        &mut tar,
        format!("{}/templates/deployment.yaml", &app_id).as_str(),
        data.as_bytes(),
    )?;
    let data = multidoc_serialize(app.services)?;
    write_file(
        &mut tar,
        format!("{}/templates/service.yaml", &app_id).as_str(),
        data.as_bytes(),
    )?;
    let data = multidoc_serialize(app.cronjobs)?;
    write_file(
        &mut tar,
        format!("{}/templates/cronjob.yaml", &app_id).as_str(),
        data.as_bytes(),
    )?;
    let data = multidoc_serialize(app.jobs)?;
    write_file(
        &mut tar,
        format!("{}/templates/job.yaml", &app_id).as_str(),
        data.as_bytes(),
    )?;
    let data = multidoc_serialize(app.pvcs)?;
    write_file(
        &mut tar,
        format!("{}/templates/pvc.yaml", &app_id).as_str(),
        data.as_bytes(),
    )?;
    let cluster_roles = app
        .service_accounts
        .iter()
        .map(|sa| sa.cluster_role.clone());
    let data = multidoc_serialize(cluster_roles.collect())?;
    write_file(
        &mut tar,
        format!("{}/templates/cluster_role.yaml", &app_id).as_str(),
        data.as_bytes(),
    )?;
    let cluster_role_bindings = app
        .service_accounts
        .iter()
        .map(|sa| sa.cluster_role_binding.clone());
    let data = multidoc_serialize(cluster_role_bindings.collect())?;
    write_file(
        &mut tar,
        format!("{}/templates/cluster_role_binding.yaml", &app_id).as_str(),
        data.as_bytes(),
    )?;
    let service_accounts = app.service_accounts.into_iter().map(|sa| sa.account);
    let data = multidoc_serialize(service_accounts.collect())?;
    write_file(
        &mut tar,
        format!("{}/templates/service_account.yaml", &app_id).as_str(),
        data.as_bytes(),
    )?;
    let data = multidoc_serialize(app.middlewares)?;
    write_file(
        &mut tar,
        format!("{}/templates/middleware.yaml", &app_id).as_str(),
        data.as_bytes(),
    )?;
    let data = multidoc_serialize(app.secrets)?;
    write_file(
        &mut tar,
        format!("{}/templates/secret.yaml", &app_id).as_str(),
        data.as_bytes(),
    )?;
    let data = multidoc_serialize(app.others)?;
    write_file(
        &mut tar,
        format!("{}/templates/others.yaml", &app_id).as_str(),
        data.as_bytes(),
    )?;
    // into_inner calls finish() internally
    let gz_encoder = tar.into_inner()?;
    let output = gz_encoder.finish()?;
    Ok(output)
}

pub async fn upload_chart(
    app: KubernetesConfig,
    metadata: OutputMetadata,
    user: &str,
    chartmuseum_url: &str,
) -> Result<()> {
    let chart = generate_chart(app, metadata, user)?;
    let full_url = format!("{}/api/charts", chartmuseum_url);
    let client = reqwest::Client::new();
    let response = client
        .post(full_url)
        .body(chart)
        .header("Content-Type", "application/gzip")
        .send()
        .await?;
    if response.status().is_success() {
        Ok(())
    } else {
        let resp = response.text().await?;
        // If data.error is "file already exists", we can ignore the error
        let data: serde_json::Value = serde_json::from_str(&resp)?;
        if data.as_object().is_some_and(|o| {
            o.get("error")
                .is_some_and(|e| e.as_str().is_some_and(|s| s == "file already exists"))
        }) {
            return Ok(());
        }
        tracing::error!("Failed to upload chart: {:?}", resp);
        Err(anyhow::anyhow!("Failed to upload chart"))
    }
}
