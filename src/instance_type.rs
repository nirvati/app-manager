use std::fmt::Display;
use std::str::FromStr;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum InstanceType {
    // Lite is for personal use on devices with limited resources, such as a Raspberry Pi 4 or the Radxa CM3
    // It uses Longhorn Engine v1
    Lite,
    // Home is for personal use on devices with more resources, such as a Raspberry Pi 5, Nabi hardware, or Radxa CM5
    // It uses Longhorn Engine v2
    Home,
}

impl FromStr for InstanceType {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "lite" => Ok(InstanceType::Lite),
            "home" => Ok(InstanceType::Home),
            _ => Err(format!("{} is not a valid instance type", s)),
        }
    }
}

impl Display for InstanceType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = match self {
            InstanceType::Lite => "Lite".to_string(),
            InstanceType::Home => "Home".to_string(),
        };
        write!(f, "{}", str)
    }
}
