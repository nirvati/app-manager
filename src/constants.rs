// The minimal app-manager version we're backwards-compatible with
pub const MINIMUM_COMPATIBLE_APP_MANAGER: &str = "0.0.1";
pub const RESERVED_NAMES: [&str; 1] = ["builtins"];
