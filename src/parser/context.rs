mod plugins;

use crate::instance_type::InstanceType;
use crate::plugins::api::UserState;
use std::collections::HashMap;
use std::path::Path;
use tera::Context;

pub async fn get_processing_context(
    apps_root: &Path,
    app_id: &str,
    instance_type: &InstanceType,
    perms: &[String],
    additional_metadata: HashMap<String, HashMap<String, serde_json::Value>>,
    user_state: UserState,
) -> anyhow::Result<Context> {
    let mut tera_ctx = Context::new();
    tera_ctx.insert("installed_apps", &user_state.installed_apps);
    tera_ctx.insert("user", &user_state.user);
    tera_ctx.insert("instance_type", &instance_type.to_string());

    if let Some(settings) = user_state.app_settings.get(app_id) {
        let parsed_settings: serde_json::Value = serde_json::from_str(settings)?;
        tera_ctx.insert("settings", &parsed_settings);
    }

    let plugins =
        plugins::call_plugins(apps_root, app_id, perms, additional_metadata, user_state).await;
    tera_ctx.insert("plugins", &plugins);
    Ok(tera_ctx)
}
