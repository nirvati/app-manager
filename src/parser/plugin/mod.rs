use crate::generator::internal::types::{InternalAppRepresentation, PluginType};
use crate::generator::metadata::types::Runtime;
use crate::manage::files::read_plugins_json;
use crate::plugins::api::UserState;
use anyhow::anyhow;
use futures::stream::FuturesUnordered;
use std::path::Path;
use tokio_stream::StreamExt;

pub async fn load_app(
    apps_root: &Path,
    app_root: &Path,
    user_state: UserState,
) -> anyhow::Result<InternalAppRepresentation> {
    let app_dir = crate::utils::tar::compress_dir(app_root).await?;
    let app_id = app_root
        .file_name()
        .ok_or_else(|| anyhow!("Failed to get app id"))?
        .to_str()
        .ok_or_else(|| anyhow!("Failed to get app id"))?
        .to_owned();
    let plugins = read_plugins_json(apps_root).await?;
    let threads = plugins
        .into_iter()
        .filter(|(_, plugin)| plugin.r#type == PluginType::Runtime)
        .map(|(plugin_id, plugin)| async {
            let mut plugin_endpoint =
                crate::plugins::api::runtime_plugin_client::RuntimePluginClient::connect(
                    plugin.endpoint,
                )
                .await?;
            let resp = plugin_endpoint
                .is_supported_app(crate::plugins::api::IsSupportedAppRequest {
                    app_id: app_id.clone(),
                    app_directory: app_dir.clone(),
                    user_state: Some(user_state.clone()),
                })
                .await?
                .into_inner();
            if resp.supported {
                let parsed = plugin_endpoint
                    .parse_app(crate::plugins::api::ParseAppRequest {
                        app_id: app_id.clone(),
                        app_directory: app_dir.clone(),
                        user_state: Some(user_state.clone()),
                    })
                    .await?
                    .into_inner();
                let mut app: InternalAppRepresentation = serde_json::from_str(&parsed.parsed_app)?;
                app.set_runtime(Runtime::Plugin(plugin_id));
                Ok(Some(app))
            } else {
                Ok(None)
            }
        });
    let mut futures_unordered = FuturesUnordered::from_iter(threads);
    let mut return_err = None;
    while let Some(result) = futures_unordered.next().await {
        match result {
            Ok(Some(app)) => return Ok(app),
            Ok(None) => continue,
            Err(err) => return_err = Some(err),
        }
    }
    if let Some(err) = return_err {
        Err(err)
    } else {
        Err(anyhow!("No plugins found for app"))
    }
}
