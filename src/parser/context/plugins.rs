use std::collections::HashMap;
use std::path::Path;

use tonic::Request;

use crate::constants::RESERVED_NAMES;
use crate::generator::internal::types::PluginType;
use crate::manage::files::read_plugins_json;
use crate::plugins::api::{GetContextRequest, UserState};

pub async fn call_plugins(
    apps_root: &Path,
    app_id: &str,
    perms: &[String],
    additional_metadata: HashMap<String, HashMap<String, serde_json::Value>>,
    user_state: UserState,
) -> HashMap<String, HashMap<String, serde_json::Value>> {
    let mut plugins_ctx = HashMap::new();
    // Perms are in the format <app-id>/<perm> or just <app-id>
    let mut perms_by_app = HashMap::new();
    for perm in perms {
        let mut split = perm.split('/');
        let app_id = split.next().unwrap();
        let perm = split.next().unwrap_or("");
        if RESERVED_NAMES.contains(&app_id) {
            continue;
        }
        let perms = perms_by_app
            .entry(app_id.to_owned())
            .or_insert_with(Vec::new);
        perms.push(perm.to_owned());
    }
    let plugins = read_plugins_json(apps_root).await.unwrap_or_default();
    let plugins = plugins
        .iter()
        .filter(|(k, p)| perms_by_app.contains_key(*k) && p.r#type == PluginType::Context);
    for (plugin_app, plugin_endpoint) in plugins {
        let metadata = additional_metadata.get(plugin_app);
        let serialized_metadata = if let Some(metadata) = metadata {
            serde_json::to_string(metadata).unwrap()
        } else {
            "{}".to_owned()
        };
        let mut client =
            match crate::plugins::api::context_plugin_client::ContextPluginClient::connect(
                plugin_endpoint.endpoint.clone(),
            )
            .await
            {
                Ok(client) => client,
                Err(err) => {
                    tracing::error!("Failed to connect to plugin {}: {:#}", plugin_app, err);
                    continue;
                }
            };
        let additions = match client
            .get_context(Request::new(GetContextRequest {
                app_id: app_id.to_owned(),
                permissions: perms_by_app[plugin_app].clone(),
                additional_data: serialized_metadata,
                user_state: Some(user_state.clone()),
            }))
            .await
        {
            Ok(additions) => additions.into_inner(),
            Err(err) => {
                tracing::error!(
                    "Failed to get jinja context from plugin {}: {:#}",
                    plugin_app,
                    err
                );
                continue;
            }
        };
        let deserialized: HashMap<String, serde_json::Value> =
            match serde_json::from_str(&additions.context) {
                Ok(deserialized) => deserialized,
                Err(err) => {
                    tracing::error!(
                        "Failed to deserialize jinja context from plugin {}: {:#}",
                        plugin_app,
                        err
                    );
                    continue;
                }
            };
        plugins_ctx.insert(plugin_app.clone(), deserialized);
    }
    plugins_ctx
}
