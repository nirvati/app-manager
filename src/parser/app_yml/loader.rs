use std::path::Path;

use crate::generator::internal::ResolverCtx;
use anyhow::{anyhow, Result};

use crate::generator::internal::types::InternalAppRepresentation;
use crate::generator::metadata::types::OutputMetadata;
use crate::instance_type::InstanceType;
use crate::parser::app_yml::types::AppYml;
use crate::parser::app_yml::types::MetadataYml;
use crate::parser::app_yml::v1;
use crate::plugins::api::UserState;

pub fn load_metadata_yml(metadata_yml: serde_yaml::Value) -> Result<MetadataYml> {
    let metadata_version = metadata_yml
        .get("version")
        .ok_or_else(|| anyhow!("metadata.yml does not contain a version"))?
        .as_i64()
        .ok_or_else(|| anyhow!("metadata.yml version is not an integer"))?;
    match metadata_version {
        1 => {
            let metadata_yml = MetadataYml::V1(serde_yaml::from_value(metadata_yml)?);
            Ok(metadata_yml)
        }
        _ => Err(anyhow!("metadata.yml version is not supported")),
    }
}

pub fn load_app_yml(app_yml: serde_yaml::Value) -> Result<AppYml> {
    let app_version = app_yml
        .get("version")
        .ok_or_else(|| anyhow!("app.yml does not contain a version"))?
        .as_i64()
        .ok_or_else(|| anyhow!("app.yml version is not an integer"))?;
    match app_version {
        1 => {
            let app_yml = AppYml::V1(serde_yaml::from_value(app_yml)?);
            Ok(app_yml)
        }
        _ => Err(anyhow!("app.yml version is not supported")),
    }
}

fn load_metadata(
    app_root: &Path,
    user_state: UserState,
    nirvati_seed: &str,
    ports: Vec<u16>,
) -> Result<OutputMetadata> {
    let app_id = app_root
        .file_name()
        .ok_or_else(|| anyhow!("Failed to get app id"))?
        .to_str()
        .ok_or_else(|| anyhow!("Failed to get app id"))?
        .to_owned();
    let metadata_yml_jinja = app_root.join("metadata.yml.jinja");
    Ok(if metadata_yml_jinja.exists() {
        let metadata_str = super::tera::process_metadata_yml_jinja(
            &metadata_yml_jinja,
            &app_id,
            nirvati_seed,
            &user_state,
        )?;
        let metadata = serde_yaml::from_str::<serde_yaml::Value>(&metadata_str)?;
        load_metadata_yml(metadata)
    } else {
        let metadata_yml = app_root.join("metadata.yml");
        let metadata =
            serde_yaml::from_str::<serde_yaml::Value>(&std::fs::read_to_string(metadata_yml)?)?;
        load_metadata_yml(metadata)
    }?
    .try_into_output_metadata(app_id, ports)?)
}

pub async fn load_app(
    apps_root: &Path,
    app_root: &Path,
    user_state: UserState,
    nirvati_seed: &str,
    instance_type: InstanceType,
    init_domain: Option<String>,
    ctx: &mut ResolverCtx,
) -> Result<InternalAppRepresentation> {
    let app_id = app_root
        .file_name()
        .ok_or_else(|| anyhow!("Failed to get app id"))?
        .to_str()
        .ok_or_else(|| anyhow!("Failed to get app id"))?
        .to_owned();
    let mut metadata = load_metadata(app_root, user_state.clone(), nirvati_seed, Vec::new())?;
    let app_yml_jinja = app_root.join("app.yml.jinja");
    let app_yml = if app_yml_jinja.exists() {
        let app_str = super::tera::process_app_yml_jinja(
            apps_root,
            &app_id,
            &app_yml_jinja,
            nirvati_seed,
            &instance_type,
            &metadata.has_permissions,
            metadata.exported_data.clone().unwrap_or_default(),
            user_state,
            init_domain,
        )
        .await?;
        let app = serde_yaml::from_str::<serde_yaml::Value>(&app_str)?;
        load_app_yml(app)?
    } else {
        let app_yml = app_root.join("app.yml");
        let app = serde_yaml::from_str::<serde_yaml::Value>(&std::fs::read_to_string(app_yml)?)?;
        load_app_yml(app)?
    };
    metadata.ports = app_yml.get_ports();

    Ok(match app_yml {
        AppYml::V1(app_yml) => v1::convert::convert_app_yml(&app_id, &app_yml, &metadata, ctx).await?,
    })
}
