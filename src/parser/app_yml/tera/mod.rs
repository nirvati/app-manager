use std::{collections::HashMap, path::Path, time::Duration};

use anyhow::{anyhow, Result};
use tera::{Context, Tera};

use crate::instance_type::InstanceType;
use crate::parser::context::get_processing_context;
use crate::plugins::api::UserState;

mod builtins;
pub mod js;

pub fn render_sandboxed(
    tera: Tera,
    file_name: String,
    code: String,
    contents: String,
    functions: Vec<String>,
    ctx: Context,
) -> Result<String> {
    let (tx, rx) = std::sync::mpsc::channel();
    let thread = std::thread::spawn(move || -> Result<()> {
        // This may execute JS code, so we need to sandbox it
        extrasafe_multiarch::SafetyContext::new()
            .enable(
                extrasafe_multiarch::builtins::SystemIO::nothing()
                    .allow_stdout()
                    .allow_stderr(),
            )
            .unwrap()
            .apply_to_current_thread()?;

        let mut tera = js::TeraWithJs::new(tera, &code, &functions)?;
        let result = tera.render_str(file_name, &contents, &ctx);
        tx.send(result)?;
        Ok(())
    });
    let rendered = rx.recv_timeout(Duration::from_secs(2))??;
    thread.join().or(Err(anyhow!("Thread panicked")))??;
    Ok(rendered)
}
pub fn process_metadata_yml_jinja(
    file: &Path,
    app_id: &str,
    nirvati_seed: &str,
    user_state: &UserState,
) -> Result<String> {
    let contents = std::fs::read_to_string(file)?;
    let dir = file
        .parent()
        .ok_or_else(|| anyhow!("Failed to get parent dir"))?;

    let mut tera_ctx = Context::new();
    let mut tera = Tera::default();
    tera.functions
        .remove("get_env")
        .expect("get_env was not available in Tera, this is a bug in Nirvati!");
    builtins::register_builtins(
        &mut tera,
        nirvati_seed.to_string(),
        &user_state.user,
        app_id,
    )?;
    let tera_dir = dir.join("_tera");
    let mut code = String::new();
    let mut functions = Vec::new();
    if tera_dir.is_dir() {
        (code, functions) = js::parse_tera_helpers(&dir.join("_tera"))?;
    }
    let globals_yml = dir.join("globals.yml");
    if globals_yml.is_file() {
        let globals = std::fs::read_to_string(globals_yml)?;
        let globals = serde_yaml::from_str::<serde_json::Value>(&globals)?;
        tera_ctx.insert("globals", &globals);
    }
    let file_name = file.file_name().unwrap().to_str().unwrap().to_string();
    render_sandboxed(tera, file_name, code, contents, functions, tera_ctx)
}

pub async fn process_app_yml_jinja(
    apps_root: &Path,
    app_id: &str,
    file: &Path,
    nirvati_seed: &str,
    instance_type: &InstanceType,
    perms: &[String],
    additional_metadata: HashMap<String, HashMap<String, serde_json::Value>>,
    user_state: UserState,
    init_domain: Option<String>,
) -> Result<String> {
    let contents = std::fs::read_to_string(file)?;
    let dir = file
        .parent()
        .ok_or_else(|| anyhow!("Failed to get parent dir"))?;

    let mut tera = Tera::default();
    tera.functions
        .remove("get_env")
        .expect("get_env was not available in Tera, the API may have changed");
    builtins::register_builtins(
        &mut tera,
        nirvati_seed.to_string(),
        &user_state.user,
        app_id,
    )?;
    let tera_dir = dir.join("_tera");
    let mut code = String::new();
    let mut functions = Vec::new();
    if tera_dir.is_dir() {
        (code, functions) = js::parse_tera_helpers(&dir.join("_tera"))?;
    }
    let file_name = file.file_name().unwrap().to_str().unwrap().to_string();
    let mut tera_ctx = get_processing_context(
        apps_root,
        app_id,
        instance_type,
        perms,
        additional_metadata,
        user_state,
    )
    .await?;

    let dir = file
        .parent()
        .ok_or_else(|| anyhow!("Failed to get parent dir"))?;
    let globals_yml = dir.join("globals.yml");
    if globals_yml.is_file() {
        let globals = std::fs::read_to_string(globals_yml)?;
        let globals = serde_yaml::from_str::<serde_json::Value>(&globals)?;
        tera_ctx.insert("globals", &globals);
    }
    let init_domain = init_domain.unwrap_or_else(|| "placeholder.nirvati.local".to_string());
    tera_ctx.insert("init_domain", &init_domain);
    let rendered = render_sandboxed(tera, file_name, code, contents, functions, tera_ctx)?;
    Ok(rendered)
}
