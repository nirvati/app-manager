use std::{
    collections::HashMap,
    marker::PhantomData,
    path::Path,
    sync::{Arc, Mutex},
};

use anyhow::{anyhow, bail, Result};
use deno_ast::{ParseParams, SourceTextInfo};
use quickjs_rs::{Context as QuickJSContext, JsValue};
use rand::RngCore;
use serde_json::Value;
use tera::{Context, Tera};

pub fn get_exported_functions(path: &Path) -> Result<Vec<String>> {
    let contents = std::fs::read_to_string(path)?;
    let mut exported_funcs = Vec::new();
    let script = deno_ast::parse_script(ParseParams {
        specifier: deno_ast::ModuleSpecifier::from_file_path(path)
            .map_err(|()| anyhow!("Failed to get module specifier from file path: {:?}", path))?,
        media_type: deno_ast::MediaType::JavaScript,
        capture_tokens: false,
        maybe_syntax: None,
        scope_analysis: false,
        text_info: SourceTextInfo::new(contents.into()),
    })?;
    // Get all function names
    for node in &script.script().body {
        if let deno_ast::swc::ast::Stmt::Decl(deno_ast::swc::ast::Decl::Fn(func)) = node {
            if ((func.ident.sym.to_string().as_str() != "postprocess"
                && func.function.params.len() == 1)
                || (func.ident.sym.to_string().as_str() == "postprocess"
                    && func.function.params.len() == 2))
                && !exported_funcs.contains(&func.ident.sym.to_string())
            {
                exported_funcs.push(func.ident.sym.to_string());
            }
        }
    }
    Ok(exported_funcs)
}

pub fn parse_tera_helpers(dir: &Path) -> anyhow::Result<(String, Vec<String>)> {
    let mut code = String::new();
    let mut exported_funcs = Vec::new();
    for entry in std::fs::read_dir(dir)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_file() {
            let ext = path
                .extension()
                .ok_or_else(|| anyhow!("Failed to get extension of file"))?;
            if ext == "js" {
                // This should be safe, but run it in a separate thread without any FS access to be sure it can't be doing anything malicious
                let exported_func_additions = get_exported_functions(&path)?;
                let code_additions = std::fs::read_to_string(&path)?;
                code.push_str(&code_additions);
                exported_funcs.extend(exported_func_additions);
            }
        }
    }
    Ok((code, exported_funcs))
}

fn js_val_to_serde_val(val: JsValue) -> Result<Value> {
    Ok(match val {
        JsValue::Undefined | JsValue::Null => Value::Null,
        JsValue::Bool(bool) => Value::Bool(bool),
        JsValue::Int(i32) => Value::Number(i32.into()),
        JsValue::Float(f64) => Value::Number(
            serde_json::Number::from_f64(f64)
                .ok_or_else(|| anyhow!("Failed to convert f64 to Number"))?,
        ),
        JsValue::String(string) => Value::String(string),
        JsValue::Array(arr) => Value::Array(
            arr.into_iter()
                .map(js_val_to_serde_val)
                .collect::<Result<Vec<Value>>>()?,
        ),
        JsValue::Object(obj) => Value::Object(serde_json::Map::from_iter(
            obj.into_iter()
                .map(|(key, val)| Ok((key, js_val_to_serde_val(val)?)))
                .collect::<Result<Vec<(String, Value)>>>()?,
        )),
        JsValue::Date(date) => Value::String(date.to_rfc3339()),
        JsValue::BigInt(bigint) => Value::String(bigint.to_string()),
        _ => bail!("Failed to convert JsValue to Value"),
    })
}

fn serde_val_to_js_val(val: Value) -> JsValue {
    match val {
        Value::Null => JsValue::Null,
        Value::Bool(bool) => JsValue::Bool(bool),
        Value::Number(num) => {
            if let Some(num) = num.as_i64() {
                JsValue::Int(num as i32)
            } else if let Some(num) = num.as_f64() {
                JsValue::Float(num)
            } else {
                JsValue::Undefined
            }
        }
        Value::String(string) => JsValue::String(string),
        Value::Array(arr) => JsValue::Array(
            arr.into_iter()
                .map(serde_val_to_js_val)
                .collect::<Vec<JsValue>>(),
        ),
        Value::Object(obj) => JsValue::Object(
            obj.into_iter()
                .map(|(key, val)| (key, serde_val_to_js_val(val)))
                .collect::<HashMap<String, JsValue>>(),
        ),
    }
}

// This is a hack, but it works, at least for now.
struct CtxWrapper(QuickJSContext);
unsafe impl Send for CtxWrapper {}
unsafe impl Sync for CtxWrapper {}

pub struct TeraWithJs {
    tera: Tera,
    quickjs_ctx: Arc<Mutex<CtxWrapper>>,
    postprocess: bool,
    _not_sync: PhantomData<*mut ()>,
}

impl TeraWithJs {
    pub fn new(mut tera: Tera, code: &str, exported_funcs: &[String]) -> Result<Self> {
        let ctx_arc;
        let mut postprocess;
        {
            let ctx = QuickJSContext::new()?;
            postprocess = false;
            ctx.add_callback("_nirvati_getRandomValues", |len: i32| -> JsValue {
                let mut rng = rand::thread_rng();
                let mut bytes = vec![0u8; len as usize];
                rng.fill_bytes(&mut bytes);
                JsValue::String(hex::encode(bytes))
            })?;
            ctx.add_callback("_nirvati_dbg", |msg: String| -> JsValue {
                tracing::debug!("[JS] {}", msg);
                JsValue::Undefined
            })?;
            ctx.eval(code)?;
            ctx_arc = Arc::new(Mutex::new(CtxWrapper(ctx)));

            for func in exported_funcs {
                if func == "postprocess" {
                    postprocess = true;
                    continue;
                }
                let ctx = ctx_arc.clone();
                let fn_name = func.clone();
                tera.register_function(func, move |args: &HashMap<String, Value>| {
                    let ctx = ctx.as_ref().lock();
                    let Ok(ctx) = ctx else {
                        return Err("Failed to lock context".into());
                    };
                    let result = ctx.0.call_function(
                        &fn_name,
                        vec![JsValue::Object(
                            args.iter()
                                .map(|(key, val)| {
                                    let val = serde_val_to_js_val(val.clone());
                                    Ok((key.clone(), val))
                                })
                                .collect::<Result<HashMap<String, JsValue>, tera::Error>>()?,
                        )],
                    );
                    if let Ok(result) = result {
                        let result = js_val_to_serde_val(result);
                        if let Ok(result) = result {
                            Ok(result)
                        } else {
                            Err("Failed to convert JS value to serde value".into())
                        }
                    } else {
                        eprintln!("{:#?}", result.err());
                        Err(format!("Failed to call JS function {}", fn_name).into())
                    }
                });
            }
        }
        Ok(Self {
            tera,
            postprocess,
            quickjs_ctx: ctx_arc,
            _not_sync: PhantomData,
        })
    }

    pub fn eval(&self, code: &str) -> Result<JsValue> {
        let ctx = self.quickjs_ctx.as_ref().lock();
        let Ok(ctx) = ctx else {
            return Err(anyhow!("Failed to lock context"));
        };
        Ok(ctx.0.eval(code)?)
    }

    pub fn render_str(
        &mut self,
        filename: String,
        input: &str,
        context: &Context,
    ) -> Result<String> {
        let mut res = self.tera.render_str(input, context)?;
        if self.postprocess {
            let ctx = self
                .quickjs_ctx
                .as_ref()
                .lock()
                .ok()
                .ok_or(anyhow!("Failed to lock context"))?;
            res = ctx
                .0
                .call_function(
                    "postprocess",
                    vec![JsValue::String(filename), JsValue::String(res)],
                )?
                .into_string()
                .ok_or(anyhow!("Failed to convert postprocess result to string"))?;
        }
        Ok(res)
    }
}

// TODO: Wait for this to be in stable Rust
//impl !Send for TeraWithJs {}
//impl !Sync for TeraWithJs {}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use quickjs_rs::JsValue;
    use serde_json::Value;
    use tera::Tera;

    use super::TeraWithJs;

    #[test]
    fn test_js_execution() {
        let code = r#"
            function math(args) {
                return (args.num1 + 1) * args.num2;
            }"#;
        let mut tera = TeraWithJs::new(Tera::default(), code, &["math".to_string()]).unwrap();
        let result = tera
            .tera
            .render_str("{{ math(num1=5, num2=2) }}", &tera::Context::new())
            .unwrap();
        assert_eq!(result, "12");
    }

    #[test]
    fn test_async_js_execution() {
        let code = r#"
            async function async_math(args) {
                return new Promise((resolve) => {
                    resolve((args.num1 + 1) * args.num2);
                });
            }"#;
        let mut tera = TeraWithJs::new(Tera::default(), code, &["async_math".to_string()]).unwrap();
        let result = tera
            .tera
            .render_str("{{ async_math(num1=5, num2=2) }}", &tera::Context::new())
            .unwrap();
        assert_eq!(result, "12");
    }

    #[test]
    fn test_js_val_to_serde_val() {
        use super::js_val_to_serde_val;
        let val = JsValue::Object(HashMap::<String, JsValue>::from_iter(vec![
            ("num".to_string(), JsValue::Int(5)),
            ("str".to_string(), JsValue::String("hello".to_string())),
            ("bool".to_string(), JsValue::Bool(true)),
            ("float".to_string(), JsValue::Float(5.5)),
            ("null".to_string(), JsValue::Null),
            ("undefined".to_string(), JsValue::Undefined),
            (
                "array".to_string(),
                JsValue::Array(vec![
                    JsValue::Int(1),
                    JsValue::Int(2),
                    JsValue::Int(3),
                    JsValue::Object(HashMap::<String, JsValue>::from_iter(vec![
                        ("num".to_string(), JsValue::Int(5)),
                        ("str".to_string(), JsValue::String("hello".to_string())),
                        ("bool".to_string(), JsValue::Bool(true)),
                        ("float".to_string(), JsValue::Float(5.5)),
                        ("null".to_string(), JsValue::Null),
                        ("undefined".to_string(), JsValue::Undefined),
                    ])),
                ]),
            ),
            (
                "object".to_string(),
                JsValue::Object(HashMap::<String, JsValue>::from_iter(vec![
                    ("num".to_string(), JsValue::Int(5)),
                    ("str".to_string(), JsValue::String("hello".to_string())),
                    ("bool".to_string(), JsValue::Bool(true)),
                    ("float".to_string(), JsValue::Float(5.5)),
                    ("null".to_string(), JsValue::Null),
                    ("undefined".to_string(), JsValue::Undefined),
                ])),
            ),
        ]));
        let result = js_val_to_serde_val(val).unwrap();
        let expected = Value::Object(
            vec![
                ("num".to_string(), Value::Number(5.into())),
                ("str".to_string(), Value::String("hello".to_string())),
                ("bool".to_string(), Value::Bool(true)),
                (
                    "float".to_string(),
                    Value::Number(serde_json::Number::from_f64(5.5).unwrap()),
                ),
                ("null".to_string(), Value::Null),
                ("undefined".to_string(), Value::Null),
                (
                    "array".to_string(),
                    Value::Array(vec![
                        Value::Number(1.into()),
                        Value::Number(2.into()),
                        Value::Number(3.into()),
                        Value::Object(serde_json::Map::from_iter(vec![
                            ("num".to_string(), Value::Number(5.into())),
                            ("str".to_string(), Value::String("hello".to_string())),
                            ("bool".to_string(), Value::Bool(true)),
                            (
                                "float".to_string(),
                                Value::Number(serde_json::Number::from_f64(5.5).unwrap()),
                            ),
                            ("null".to_string(), Value::Null),
                            ("undefined".to_string(), Value::Null),
                        ])),
                    ]),
                ),
                (
                    "object".to_string(),
                    Value::Object(serde_json::Map::from_iter(vec![
                        ("num".to_string(), Value::Number(5.into())),
                        ("str".to_string(), Value::String("hello".to_string())),
                        ("bool".to_string(), Value::Bool(true)),
                        (
                            "float".to_string(),
                            Value::Number(serde_json::Number::from_f64(5.5).unwrap()),
                        ),
                        ("null".to_string(), Value::Null),
                        ("undefined".to_string(), Value::Null),
                    ])),
                ),
            ]
            .into_iter()
            .collect(),
        );
        assert_eq!(result, expected);
    }
}
