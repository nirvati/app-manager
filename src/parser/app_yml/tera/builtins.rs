use std::collections::HashMap;

use anyhow::Result;
use tera::Tera;

use crate::utils::derive_entropy;

pub fn register_builtins(
    tera: &mut Tera,
    nirvati_seed: String,
    user_id: &str,
    app_id: &str,
) -> Result<()> {
    let app_id = app_id.to_string();
    let user_id = user_id.to_string();
    tera.register_function(
        "derive_entropy",
        move |args: &HashMap<String, tera::Value>| -> tera::Result<tera::Value> {
            let identifier = args
                .get("identifier")
                .ok_or_else(|| tera::Error::msg("identifier not provided"))?
                .as_str()
                .ok_or_else(|| tera::Error::msg("identifier is not a string"))?;
            Ok(tera::Value::String(derive_entropy(
                &nirvati_seed,
                format!("{}:{}:{}", user_id, app_id, identifier).as_str(),
            )))
        },
    );
    Ok(())
}
