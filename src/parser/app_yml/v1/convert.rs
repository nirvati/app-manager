use std::collections::BTreeMap;

use anyhow::{bail, Result};
use base64::prelude::*;
use lazy_static::lazy_static;
use regex::Regex;
use slugify::slugify;

use crate::generator::internal::types::{
    Container as OutContainer, HostVolume, Ingress, LonghornVolume, PluginInfo,
    Runnable as OutRunnable, Secret, SecretMount, Service as OutService,
    ServiceAccount as OutServiceAccount, ServiceType,
};
use crate::generator::internal::types::{InternalAppRepresentation, Volume};
use crate::generator::internal::{types as output, ResolverCtx};
use crate::generator::metadata::types::OutputMetadata;
use crate::parser::app_yml::v1::types::app::{Container, IngressTarget, Job, Runnable};

use super::types::AppYml;

lazy_static! {
    static ref PATH_PREFIX_VALIDATION: Regex = Regex::new(r"^[A-z/\-_0-9]+$").unwrap();
}

fn validate_path_prefix(path_prefix: &str) -> Result<()> {
    if !PATH_PREFIX_VALIDATION.is_match(path_prefix) {
        bail!("Path prefix {} is invalid!", path_prefix);
    }
    Ok(())
}

pub fn convert_mounts(
    app_id: &str,
    result: &mut OutContainer,
    input_service: &Container,
    metadata: &OutputMetadata,
) -> Result<()> {
    for (volume_path, container_dir) in &input_service.mounts.data {
        let name = volume_path.split('/').next().unwrap().to_owned();
        if name.is_empty() || name.len() > 63 {
            bail!("Volume name {} is invalid!", name);
        }
        if !metadata.volumes.contains_key(&name) {
            bail!(
                "Volume {} does not exist, mounted by {}",
                volume_path,
                app_id
            );
        };
        result.volumes.push(Volume::OwnLonghorn(LonghornVolume {
            name: name.clone(),
            mount_path: container_dir.to_owned(),
            sub_path: if volume_path.contains('/') {
                Some(
                    volume_path
                        .split('/')
                        .skip(1)
                        .collect::<Vec<_>>()
                        .to_owned()
                        .join("/"),
                )
            } else {
                None
            },
            is_readonly: false,
        }))
    }
    for (secret_name, container_dir) in &input_service.mounts.secrets {
        result.volumes.push(Volume::Secret(SecretMount {
            name: secret_name.clone(),
            mount_path: container_dir.to_owned(),
        }))
    }
    for (host_dir, container_dir) in &input_service.mounts.host_paths {
        let name = slugify!(host_dir);
        result.volumes.push(Volume::Host(HostVolume {
            name: name.clone(),
            mount_path: container_dir.to_owned(),
            host_path: host_dir.to_owned(),
            is_readonly: false,
        }))
    }
    Ok(())
}

pub async fn convert_app_yml(
    app_id: &str,
    app_yml: &AppYml,
    metadata: &OutputMetadata,
    ctx: &mut ResolverCtx,
) -> Result<InternalAppRepresentation> {
    let mut services = BTreeMap::new();
    let mut containers = BTreeMap::new();
    let mut ingress = Vec::new();
    let mut plugins = Vec::new();
    let mut secrets = Vec::new();
    for (secret_name, secret) in &app_yml.secrets {
        let mut data: BTreeMap<String, Box<[u8]>> = secret
            .data
            .iter()
            .filter_map(|(k, v)| match BASE64_STANDARD.decode(v) {
                Ok(bytes) => Some((k.to_owned(), bytes.into_boxed_slice())),
                Err(decoding_error) => {
                    tracing::warn!(
                        "Failed to decode secret {} of app {}: {}. If you are passing a non-base64 string, please put it into string_data instead of data.",
                        secret_name,
                        app_id,
                        decoding_error
                    );
                    None
                }
            })
            .collect();
        for (key, val) in secret.string_data.iter() {
            data.insert(key.to_owned(), val.as_bytes().to_owned().into_boxed_slice());
        }
        secrets.push(Secret {
            name: secret_name.to_owned(),
            data,
        });
    }
    let ingress_target = app_yml.ingress_target.clone().unwrap_or(IngressTarget {
        name: "main".to_string(),
        target_app: None,
        port: None,
    });
    // If ingress is within this app, ensure the service we're targeting exists
    if !ingress_target
        .target_app
        .as_ref()
        .is_some_and(|target_app| target_app != app_id)
        && !app_yml.containers.contains_key(&ingress_target.name)
        && !app_yml.services.contains_key(&ingress_target.name)
    {
        bail!(
            "Ingress target {} does not exist in app {}",
            ingress_target.name,
            app_id
        );
    }
    for (container_id, runnable) in &app_yml.containers {
        let is_ingress_target = container_id == &ingress_target.name
            && (ingress_target
                .target_app
                .as_ref()
                .is_some_and(|target_app| target_app == app_id)
                || ingress_target.target_app.is_none());
        let container = runnable.get_container();
        if let Runnable::Deployment(ref deployment) = runnable {
            let mut internal_ports: output::Ports = deployment.internal_ports.clone().into();
            if let Some(main_port) = deployment.ingress_port {
                internal_ports.tcp.entry(main_port).or_insert(main_port);
                if let Some(ref path_prefix) = deployment.path_prefix {
                    if is_ingress_target {
                        bail!("Main service is not allowed to have a path prefix!");
                    }
                    validate_path_prefix(path_prefix)?;
                }
                if deployment.path_prefix.is_some() || is_ingress_target {
                    ingress.push(Ingress {
                        target_service: container_id.to_owned(),
                        path_prefix: deployment.path_prefix.clone(),
                        target_port: main_port,
                        enable_compression: container.enable_http_compression,
                        strip_prefix: container.strip_prefix,
                        target_app: None,
                    });
                }
            };
            for plugin in &deployment.plugins {
                plugins.push(PluginInfo {
                    endpoint: format!(
                        "http://{}.{}.svc.cluster.local:{}{}",
                        container_id,
                        app_id,
                        plugin.port,
                        plugin.path.clone().unwrap_or("/".to_owned())
                    ),
                    r#type: plugin.r#type.into(),
                    name: plugin.name.clone(),
                });
            }

            if !internal_ports.is_empty() {
                services.insert(
                    container_id.clone(),
                    OutService {
                        target_container: container_id.clone(),
                        name: container_id.clone(),
                        r#type: ServiceType::ClusterIp,
                        ports: internal_ports,
                    },
                );
            }
            if !deployment.public_ports.is_empty() {
                let svc_name = container_id.clone() + "-public";
                if services.contains_key(&svc_name) {
                    bail!("Implicitly created services for public ports use the -public suffix, but a service named {} already exists!", svc_name);
                }
                services.insert(
                    svc_name.clone(),
                    OutService {
                        target_container: container_id.clone(),
                        name: svc_name,
                        r#type: ServiceType::LoadBalancer,
                        ports: deployment.public_ports.clone().into(),
                    },
                );
            }
        }
        let mut result_service = OutContainer {
            image: container.image.clone(),
            restart: container.restart.clone(),
            stop_grace_period: container.stop_grace_period.clone(),
            uid: container
                .user
                .clone()
                .map(|p| p.split(':').next().unwrap().parse().unwrap_or(0)),
            gid: if let Some(user) = container.user.clone() {
                user.split(':').nth(1).map(|p| p.parse().unwrap())
            } else {
                None
            },
            working_dir: container.working_dir.clone(),
            shm_size: container.shm_size.clone(),
            volumes: Vec::new(),
            cap_add: container.cap_add.clone(),
            command: container.command.clone().map(|cmd| cmd.into()),
            entrypoint: container.entrypoint.clone().map(|cmd| cmd.into()),
            host_network: container.host_network,
            environment: container
                .environment
                .clone()
                .into_iter()
                .map(|(k, v)| (k, v.into()))
                .collect(),
            ulimits: container.ulimits.clone(),
            privileged: container.privileged,
            fs_group: container.fs_group,
            service_account: container
                .service_account
                .clone()
                .map(|sa| OutServiceAccount {
                    cluster_rules: sa.rules,
                    ns_rules: Default::default(),
                }),
            ..Default::default()
        };
        if let Runnable::Deployment(ref deployment) = runnable {
            result_service
                .exposes
                .append(&deployment.public_ports.clone().into());
            result_service
                .exposes
                .append(&deployment.internal_ports.clone().into());
        }

        convert_mounts(app_id, &mut result_service, container, metadata)?;
        containers.insert(
            container_id.to_owned(),
            match runnable {
                Runnable::Deployment(_) => OutRunnable::Deployment(Box::new(output::Deployment {
                    container: result_service,
                })),
                Runnable::Job(job) => match job {
                    Job::OneTime(_) => OutRunnable::Once(Box::new(output::Job {
                        container: result_service,
                    })),
                    Job::OnUpdate(_) => OutRunnable::OnAppUpdate(Box::new(output::Job {
                        container: result_service,
                    })),
                },
                Runnable::CronJob(cron_job) => OutRunnable::Cron(Box::new(output::CronJob {
                    schedule: cron_job.schedule.clone(),
                    container: result_service,
                })),
            },
        );
    }

    for (service_id, svc) in &app_yml.services {
        if services.contains_key(service_id) {
            bail!(
                "Service {} conflicts with implicitly created service of the same name.",
                service_id
            );
        }
        let OutRunnable::Deployment(ref mut target_container) =
            containers.get_mut(&svc.target_container).ok_or_else(|| {
                anyhow::anyhow!(
                    "Service {} targets non-existent container {}",
                    service_id,
                    svc.target_container
                )
            })?
        else {
            bail!(
                "Service {} targets non-deployment container {}",
                service_id,
                svc.target_container
            );
        };
        let ports: output::Ports = svc.ports.clone().into();
        target_container.container.exposes.append(&ports);
        services.insert(
            service_id.clone(),
            OutService {
                target_container: svc.target_container.clone(),
                name: service_id.clone(),
                r#type: svc.r#type.into(),
                ports,
            },
        );
        let is_ingress_target = service_id == &ingress_target.name
            && (ingress_target
                .target_app
                .as_ref()
                .is_some_and(|target_app| target_app == app_id)
                || ingress_target.target_app.is_none());
        if is_ingress_target || svc.ingress_port.is_some() {
            let Some(ingress_port) = svc.ingress_port else {
                bail!("The ingress target service must have an ingress port!");
            };
            if let Some(ref path_prefix) = svc.path_prefix {
                if is_ingress_target {
                    bail!("Main service is not allowed to have a path prefix!");
                }
                validate_path_prefix(path_prefix)?;
            }
            ingress.push(Ingress {
                target_service: service_id.to_owned(),
                path_prefix: svc.path_prefix.clone(),
                target_port: ingress_port,
                enable_compression: svc.enable_http_compression,
                strip_prefix: svc.strip_prefix,
                target_app: None,
            });
        }
    }
    for ingress_route in &app_yml.ingress {
        ingress.push(Ingress {
            target_service: ingress_route.target_svc.clone(),
            target_app: Some(ingress_route.target_app.clone()),
            path_prefix: Some(ingress_route.path_prefix.clone()),
            target_port: ingress_route.port,
            enable_compression: ingress_route.enable_http_compression,
            strip_prefix: ingress_route.strip_prefix,
        })
    }
    let mut encountered_prefixes: Vec<&str> = Vec::new();
    for ingress in &ingress {
        if let Some(ref prefix) = ingress.path_prefix {
            let normalized_prefix = prefix.trim_end_matches('/');
            if encountered_prefixes.contains(&normalized_prefix) {
                bail!("Path prefix {} is used by multiple services!", prefix);
            }
            encountered_prefixes.push(normalized_prefix);
        }
    }
    Ok(InternalAppRepresentation::new(
        metadata.clone(),
        containers,
        services.into_values().collect(),
        ingress,
        secrets,
        plugins,
        // TODO: Actually implement CRs
        Vec::new(),
        ctx,
    ).await)
}
