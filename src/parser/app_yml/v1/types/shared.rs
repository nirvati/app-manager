use std::collections::BTreeMap;
use std::fmt::{Display, Formatter};

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
#[serde(untagged)]
pub enum MultiLanguageItem {
    String(String),
    Map(BTreeMap<String, String>),
}

impl Default for MultiLanguageItem {
    fn default() -> Self {
        MultiLanguageItem::String("".to_string())
    }
}

impl From<MultiLanguageItem> for BTreeMap<String, String> {
    fn from(val: MultiLanguageItem) -> Self {
        match val {
            MultiLanguageItem::String(s) => {
                let mut map = BTreeMap::new();
                map.insert("en".to_string(), s);
                map
            }
            MultiLanguageItem::Map(m) => m,
        }
    }
}

impl Display for MultiLanguageItem {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            MultiLanguageItem::String(s) => write!(f, "{}", s),
            MultiLanguageItem::Map(m) => {
                let system_lang = std::env::var("LANG").unwrap_or_else(|_| "en_US".to_string());
                let lang = system_lang.split('.').next().unwrap_or("en");
                if let Some(lang_item) = m.get(lang) {
                    write!(f, "{}", lang_item)
                } else {
                    // Check if the language prefix is in the map
                    let lang = lang.split('_').next().unwrap_or("en");
                    if let Some(lang_item) = m.get(lang) {
                        write!(f, "{}", lang_item)
                    } else if let Some(lang_item) = m.get("en") {
                        write!(f, "{}", lang_item)
                    } else if let Some((_, lang_item)) = m.first_key_value() {
                        write!(f, "{}", lang_item)
                    } else {
                        write!(f, "")
                    }
                }
            }
        }
    }
}
