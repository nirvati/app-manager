use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

use crate::generator::metadata::types::Permission as OutputPermission;

#[derive(Serialize, Deserialize, Clone, Default, Debug, PartialEq, Eq)]
pub struct Permission {
    pub id: String,
    pub name: crate::utils::MultiLanguageItem,
    pub description: crate::utils::MultiLanguageItem,
    /// Other permissions this permission implies
    /// May also contain permissions of other apps
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub includes: Vec<String>,
    /// Secrets (+ keys) accessible with this permission
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub secrets: BTreeMap<String, Vec<String>>,
    /// Makes this permission "invisible" (Hidden from the UI) if requested by the apps listed in this field
    /// The * wildcard can be used to hide from all apps
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub hidden: Vec<String>,
}

impl From<Permission> for OutputPermission {
    fn from(value: Permission) -> Self {
        OutputPermission {
            id: value.id,
            name: value.name,
            description: value.description,
            includes: value.includes,
            secrets: value.secrets,
            hidden: value.hidden,
        }
    }
}
