use anyhow::anyhow;
use serde::{Deserialize, Serialize};

use crate::utils::split_with_quotes;

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, Hash)]
#[serde(untagged)]
pub enum Command {
    SimpleCmd(String),
    ArraySyntax(Vec<String>),
}

impl Default for Command {
    fn default() -> Self {
        Command::ArraySyntax(Vec::new())
    }
}

impl From<String> for Command {
    fn from(s: String) -> Self {
        Command::SimpleCmd(s)
    }
}

impl From<&str> for Command {
    fn from(s: &str) -> Self {
        Command::SimpleCmd(s.to_string())
    }
}

impl From<Vec<String>> for Command {
    fn from(s: Vec<String>) -> Self {
        Command::ArraySyntax(s)
    }
}

impl From<&[String]> for Command {
    fn from(s: &[String]) -> Self {
        Command::ArraySyntax(s.to_vec())
    }
}

impl From<Command> for Vec<String> {
    fn from(cmd: Command) -> Self {
        match cmd {
            Command::SimpleCmd(cmd) => split_with_quotes(&cmd),
            Command::ArraySyntax(cmd) => cmd,
        }
    }
}

impl TryFrom<Command> for String {
    type Error = anyhow::Error;

    fn try_from(cmd: Command) -> anyhow::Result<Self, Self::Error> {
        match cmd {
            Command::SimpleCmd(cmd) => Ok(cmd),
            Command::ArraySyntax(cmd) => Err(anyhow!(
                "Cannot convert array syntax command to string: {:?}",
                cmd
            )),
        }
    }
}
