mod permissions;

use std::collections::{BTreeMap, HashMap};

use serde::{Deserialize, Serialize};

use crate::generator;
use crate::parser::app_yml::v1::types::metadata::permissions::Permission;
use crate::utils::{false_default, si_to_bytes, true_default};

use super::shared::MultiLanguageItem;

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, Hash)]
#[serde(untagged)]
pub enum Dependency {
    OneDependency(String),
    AlternativeDependency(Vec<String>),
}

impl From<Dependency> for generator::metadata::types::Dependency {
    fn from(dependency: Dependency) -> Self {
        match dependency {
            Dependency::OneDependency(dep) => {
                generator::metadata::types::Dependency::OneDependency(dep)
            }
            Dependency::AlternativeDependency(deps) => {
                generator::metadata::types::Dependency::AlternativeDependency(deps)
            }
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
pub enum SettingType {
    Enum,
    String,
    Bool,
}

impl From<SettingType> for generator::metadata::types::SettingType {
    fn from(val: SettingType) -> Self {
        match val {
            SettingType::Enum => generator::metadata::types::SettingType::Enum,
            SettingType::String => generator::metadata::types::SettingType::String,
            SettingType::Bool => generator::metadata::types::SettingType::Bool,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct SettingsDeclaration {
    #[serde(rename = "type")]
    pub setting_type: SettingType,
    #[serde(default = "Vec::default")]
    #[serde(skip_serializing_if = "Vec::<String>::is_empty")]
    pub values: Vec<String>,
    pub name: MultiLanguageItem,
    pub description: MultiLanguageItem,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub default: Option<String>,
}

impl From<SettingsDeclaration> for crate::generator::metadata::types::SettingsDeclaration {
    fn from(value: SettingsDeclaration) -> Self {
        Self {
            setting_type: value.setting_type.into(),
            values: value.values,
            name: value.name.into(),
            description: value.description.into(),
            default: value.default,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct InputMetadata {
    /// The name of the app
    pub name: String,
    /// The version of the app
    #[serde(deserialize_with = "deserialize_version")]
    pub version: semver::Version,
    /// The version of the app to display
    /// This is useful if the app is not using semver
    pub display_version: Option<String>,
    /// The category for the app
    pub category: MultiLanguageItem,
    /// A short tagline for the app
    pub tagline: MultiLanguageItem,
    // Developer name -> their website
    pub developers: BTreeMap<String, String>,
    /// A description of the app
    pub description: MultiLanguageItem,
    #[serde(default)]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    /// Other apps this app depends on
    pub dependencies: Vec<Dependency>,
    /// App repository name -> repo URL
    pub repos: BTreeMap<String, String>,
    /// A support link for the app
    pub support: String,
    /// A list of promo images for the apps
    #[serde(default)]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub gallery: Vec<String>,
    /// The URL to the app icon
    pub icon: Option<String>,
    /// The path the "Open" link on the dashboard should lead to
    #[serde(skip_serializing_if = "Option::is_none")]
    pub path: Option<String>,
    /// The app's default username
    #[serde(skip_serializing_if = "Option::is_none")]
    pub default_username: Option<String>,
    /// The app's default password.
    pub default_password: Option<String>,
    /// A list of containers to update automatically (still validated by the Citadel team)
    #[serde(skip_serializing_if = "Option::is_none")]
    pub update_containers: Option<Vec<String>>,
    /// For "virtual" apps, the service the app implements
    #[serde(skip_serializing_if = "Option::is_none")]
    pub implements: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub version_control: Option<String>,
    #[serde(
        default,
        skip_serializing_if = "BTreeMap::<String, MultiLanguageItem>::is_empty"
    )]
    pub release_notes: BTreeMap<String, MultiLanguageItem>,
    /// Permissions this app requests
    /// This will be used as a "guide" for the app manager if an app requests access to a resource
    /// That is provided by multiple permissions
    #[serde(
        default = "Vec::default",
        skip_serializing_if = "Vec::<String>::is_empty"
    )]
    pub has_permissions: Vec<String>,
    #[serde(default = "Vec::default", skip_serializing_if = "Vec::is_empty")]
    pub exposes_permissions: Vec<Permission>,
    /// The SPDX identifier of the app license
    #[serde(default = "unknown_license")]
    pub license: String,
    /// Config files to automatically overwrite when updating
    #[serde(skip_serializing_if = "Vec::is_empty", default)]
    pub update_config_files: Vec<String>,
    /// Exported data shared with plugins (Map plugin -> data)
    #[serde(skip_serializing_if = "Option::is_none")]
    pub exported_data: Option<HashMap<String, HashMap<String, serde_json::Value>>>,
    /// Whether this app should only allow one https domain
    #[serde(default = "false_default")]
    pub single_https_domain: bool,
    /// Whether this app allows the user to change the domain
    #[serde(default = "true_default")]
    pub allow_domain_change: bool,
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub settings: BTreeMap<String, SettingsDeclaration>,
    /// Volumes this app uses (Can be mounted in mounts.data)
    /// Volumes need to be declared explicitly, and have a default size limit of 10Gi
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    pub volumes: HashMap<String, Volume>,
}

pub fn deserialize_version<'de, D>(deserializer: D) -> Result<semver::Version, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    let s = s.trim_start_matches('v');
    semver::Version::parse(s).map_err(serde::de::Error::custom)
}

pub fn unknown_license() -> String {
    "Unknown".to_string()
}

#[derive(Serialize, Deserialize, Clone, Default, Debug, PartialEq, Eq)]
pub struct Volume {
    pub minimum_size: String,
    pub recommended_size: String,
    pub name: MultiLanguageItem,
    pub description: MultiLanguageItem,
}

impl TryFrom<Volume> for generator::metadata::types::Volume {
    type Error = std::io::Error;

    fn try_from(val: Volume) -> Result<Self, Self::Error> {
        Ok(generator::metadata::types::Volume {
            minimum_size: si_to_bytes(&val.minimum_size)?,
            recommended_size: si_to_bytes(&val.recommended_size)?,
            name: crate::utils::MultiLanguageItem(val.name.into()),
            description: crate::utils::MultiLanguageItem(val.description.into()),
        })
    }
}
