use std::collections::HashMap;
use std::fmt;

use itertools::Itertools;
use serde::de::{MapAccess, Visitor};
use serde::{Deserialize, Deserializer, Serialize};

use crate::generator::internal;

#[derive(Serialize, Clone, PartialEq, Debug, Default)]
pub struct Ports {
    pub udp: HashMap<u16, u16>,
    pub tcp: HashMap<u16, u16>,
}

#[derive(Deserialize, Clone, Debug, PartialEq)]
#[serde(untagged)]
enum StrOrU16 {
    Str(String),
    U16(u16),
}

struct PortsVisitor;

impl<'de> Visitor<'de> for PortsVisitor {
    type Value = Ports;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a map of ports")
    }

    fn visit_map<M>(self, mut access: M) -> Result<Self::Value, M::Error>
    where
        M: MapAccess<'de>,
    {
        let mut ports = Ports::default();
        while let Some(key) = access.next_key::<StrOrU16>()? {
            match key {
                StrOrU16::Str(protocol) => {
                    let new_ports: HashMap<u16, u16> = access.next_value()?;
                    match protocol.as_str() {
                        "tcp" => {
                            ports.tcp.extend(new_ports);
                        }
                        "udp" => {
                            ports.udp.extend(new_ports);
                        }
                        _ => {
                            return Err(serde::de::Error::custom(format!(
                                "Invalid protocol: {}",
                                protocol
                            )));
                        }
                    }
                }
                StrOrU16::U16(port) => {
                    ports.tcp.insert(port, port);
                }
            }
        }
        Ok(ports)
    }
}

impl<'de> Deserialize<'de> for Ports {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_map(PortsVisitor)
    }
}

impl Ports {
    pub fn keys(&self) -> Vec<u16> {
        return self
            .tcp
            .keys()
            .copied()
            .chain(self.udp.clone().keys().copied())
            .sorted()
            .dedup()
            .collect();
    }

    pub fn is_empty(&self) -> bool {
        self.udp.is_empty() && self.tcp.is_empty()
    }
}

impl From<Ports> for internal::types::Ports {
    fn from(value: Ports) -> Self {
        Self {
            tcp: value.tcp,
            udp: value.udp,
        }
    }
}

#[cfg(test)]
mod tests {
    use serde_yaml::from_str;

    use crate::parser::app_yml::v1::types::app::ports::Ports;

    #[test]
    fn test_ports_deserialization() {
        let yaml = r#"
        udp:
          30: 30
        tcp:
          30: 30
        40: 40
        "#;

        let ports: Ports = from_str(yaml).unwrap();

        assert_eq!(ports.udp.get(&30), Some(&30));
        assert_eq!(ports.tcp.get(&30), Some(&30));
        assert_eq!(ports.tcp.get(&40), Some(&40));
    }
}
