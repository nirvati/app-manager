use std::collections::BTreeMap;

use serde::{Deserialize, Serialize};

use crate::parser::app_yml::v1::types::app::Secret;
use crate::parser::app_yml::v1::types::metadata::InputMetadata;

pub(in crate::parser::app_yml::v1) mod app;
pub mod command;
pub(in crate::parser::app_yml::v1) mod metadata;
pub(in crate::parser::app_yml::v1) mod shared;
pub use shared::MultiLanguageItem;

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
/// Nirvati app definition
pub struct AppYml {
    pub version: u8,
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub containers: BTreeMap<String, app::Runnable>,
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub services: BTreeMap<String, app::Service>,
    // The service ingress should go towards (Default: main)
    pub ingress_target: Option<app::IngressTarget>,
    // Ingress that's supposed to go to other apps
    // If a service or container is supposed to receive ingress, it should be defined on the service or container directly
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub ingress: Vec<app::IngressRoute>,
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub secrets: BTreeMap<String, Secret>,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
/// Nirvati app metadata definition
pub struct MetadataYml {
    pub version: u8,
    pub metadata: InputMetadata,
}

impl AppYml {
    pub fn get_ports(&self) -> Vec<u16> {
        let mut ports = Vec::new();
        for service in self.services.values() {
            let mut service_ports = service.ports.keys();
            ports.append(&mut service_ports);
        }
        ports
    }
}
