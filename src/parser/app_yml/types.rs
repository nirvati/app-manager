use serde::{Deserialize, Serialize};

use crate::generator::metadata::types::{OutputMetadata, Runtime};

use super::v1::types as v1;

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum AppYml {
    V1(v1::AppYml),
}

impl AppYml {
    pub fn get_ports(&self) -> Vec<u16> {
        match self {
            AppYml::V1(app) => app.get_ports(),
        }
    }
}
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum MetadataYml {
    V1(v1::MetadataYml),
}

impl MetadataYml {
    pub fn get_implements(&self) -> &Option<String> {
        match self {
            MetadataYml::V1(metadata) => &metadata.metadata.implements,
        }
    }

    pub fn into_implements(self) -> Option<String> {
        match self {
            MetadataYml::V1(metadata) => metadata.metadata.implements,
        }
    }

    pub fn try_into_output_metadata(
        self,
        app_id: String,
        ports: Vec<u16>,
    ) -> Result<OutputMetadata, std::io::Error> {
        Ok(match self {
            MetadataYml::V1(metadata) => OutputMetadata {
                id: app_id,
                name: metadata.metadata.name,
                version: metadata.metadata.version.clone(),
                display_version: metadata
                    .metadata
                    .display_version
                    .unwrap_or_else(|| metadata.metadata.version.to_string()),
                category: metadata.metadata.category.into(),
                tagline: metadata.metadata.tagline.into(),
                developers: metadata.metadata.developers,
                description: metadata.metadata.description.into(),
                dependencies: metadata
                    .metadata
                    .dependencies
                    .into_iter()
                    .map(|dep| dep.into())
                    .collect(),
                has_permissions: metadata.metadata.has_permissions,
                repos: metadata.metadata.repos,
                support: metadata.metadata.support,
                gallery: metadata.metadata.gallery,
                icon: metadata.metadata.icon,
                path: metadata.metadata.path,
                default_username: metadata.metadata.default_username,
                default_password: metadata.metadata.default_password,
                implements: metadata.metadata.implements,
                release_notes: metadata
                    .metadata
                    .release_notes
                    .into_iter()
                    .map(|(key, val)| (key, val.into()))
                    .collect(),
                license: metadata.metadata.license,
                settings: metadata
                    .metadata
                    .settings
                    .into_iter()
                    .map(|(k, v)| (k, v.into()))
                    .collect(),
                single_https_domain: metadata.metadata.single_https_domain,
                allow_domain_change: metadata.metadata.allow_domain_change,
                volumes: metadata
                    .metadata
                    .volumes
                    .into_iter()
                    .map(|(id, vol)| Ok((id, vol.try_into()?)))
                    .collect::<Result<_, std::io::Error>>()?,
                exported_data: metadata.metadata.exported_data,
                ports,
                runtime: Runtime::AppYml,
                exposes_permissions: metadata
                    .metadata
                    .exposes_permissions
                    .into_iter()
                    .map(|p| p.into())
                    .collect(),
            },
        })
    }
}

pub use v1::MultiLanguageItem;
