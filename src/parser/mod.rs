use std::path::Path;

use anyhow::Result;
use crate::generator::internal::ResolverCtx;

use crate::generator::internal::types::InternalAppRepresentation;
use crate::instance_type::InstanceType;
use crate::plugins::api::UserState;

pub mod app_yml;
pub mod context;
mod plugin;

pub async fn process_apps(
    apps_root: &Path,
    user_state: UserState,
    nirvati_seed: &str,
    instance_type: InstanceType,
    ctx: &mut ResolverCtx,
) -> Result<Vec<InternalAppRepresentation>> {
    let mut apps = Vec::new();
    for entry in std::fs::read_dir(apps_root)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_dir() {
            let app = load_app(
                apps_root,
                path.file_name().unwrap().to_str().unwrap(),
                user_state.clone(),
                nirvati_seed,
                instance_type,
                None,
                ctx,
            )
            .await;
            match app {
                Ok(app) => apps.push(app),
                Err(err) => {
                    tracing::error!("Failed to load app: {:#}", err);
                }
            }
        }
    }
    Ok(apps)
}

pub async fn load_app(
    apps_root: &Path,
    app: &str,
    user_state: UserState,
    nirvati_seed: &str,
    instance_type: InstanceType,
    init_domain: Option<String>,
    ctx: &mut ResolverCtx,
) -> Result<InternalAppRepresentation> {
    let path = apps_root.join(app);
    if path.is_dir() {
        let has_metadata =
            path.join("metadata.yml").exists() || path.join("metadata.yml.jinja").exists();
        let has_app_yml = path.join("app.yml").exists() || path.join("app.yml.jinja").exists();
        if has_metadata && has_app_yml {
            match app_yml::load_app(
                apps_root,
                &path,
                user_state.clone(),
                nirvati_seed,
                instance_type,
                init_domain,
                ctx,
            )
            .await
            {
                Ok(result) => Ok(result),
                Err(err) => {
                    tracing::warn!(
                        "Failed to load app with builtin app.yml parser, trying plugins: {:#?}",
                        err
                    );
                    plugin::load_app(apps_root, &path, user_state).await
                }
            }
        } else {
            plugin::load_app(apps_root, &path, user_state).await
        }
    } else {
        Err(anyhow::anyhow!("App {} does not exist", app))
    }
}
