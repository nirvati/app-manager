use std::path::PathBuf;

use app_manager::generator::internal::ResolverCtx;
use kube::Client;
use tonic::Status;

use app_manager::generator::internal::types::InternalAppRepresentation;
use app_manager::instance_type::InstanceType;
use app_manager::manage::files::app_exists;
use app_manager::parser::load_app;

use crate::grpc::api::UserState;

pub mod apps;
pub mod manager;
pub mod public;
pub mod setup;

pub mod api {
    tonic::include_proto!("apps");
}

impl From<UserState> for app_manager::plugins::api::UserState {
    fn from(user_state: UserState) -> Self {
        app_manager::plugins::api::UserState {
            user: user_state.user,
            installed_apps: user_state.installed_apps,
            app_settings: user_state.app_settings,
        }
    }
}

impl From<&UserState> for app_manager::plugins::api::UserState {
    fn from(user_state: &UserState) -> Self {
        app_manager::plugins::api::UserState {
            user: user_state.user.clone(),
            installed_apps: user_state.installed_apps.clone(),
            app_settings: user_state.app_settings.clone(),
        }
    }
}

#[derive(Clone)]
pub struct ApiServer {
    pub main_apps_root: PathBuf,
    pub kube_client: Client,
    pub nirvati_seed: String,
    pub instance_type: InstanceType,
    pub chartmuseum_url: String,
}

macro_rules! expect_result {
    ($expr:expr, $msg:expr) => {
        $expr.map_err(|err| {
            tracing::error!("{}: {:#}", $msg, err);
            Status::internal($msg)
        })
    };
    ($expr:expr, $msg:expr, $type:path) => {
        $expr.map_err(|err| {
            tracing::error!("{}: {:#}", $msg, err);
            $type($msg)
        })
    };
}

macro_rules! require_opt {
    ($expr:expr) => {
        $expr.ok_or_else(|| {
            Status::invalid_argument(format!(
                "A required field was not set: {}",
                stringify!($expr)
            ))
        })
    };
}

pub(crate) use expect_result;
pub(crate) use require_opt;

impl ApiServer {
    async fn load_app(
        &self,
        app_id: &str,
        user_state: &UserState,
        init_domain: Option<String>,
    ) -> Result<InternalAppRepresentation, Status> {
        let apps_root = self.main_apps_root.join(&user_state.user);
        if !app_exists(&apps_root, app_id) {
            return Err(Status::not_found("App does not exist!"));
        }
        let mut resolver_ctx = ResolverCtx::new(
            apps_root.clone(),
            user_state.clone().into(),
            self.nirvati_seed.clone(),
            self.instance_type,
        );
        expect_result!(
            load_app(
                &apps_root,
                app_id,
                user_state.clone().into(),
                &self.nirvati_seed,
                self.instance_type,
                init_domain,
                &mut resolver_ctx,
            )
            .await,
            "Failed to load app"
        )
    }
}
