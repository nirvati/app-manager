mod context;
mod custom_resource;
mod runtime;

use crate::generator::internal::types::InternalAppRepresentation;
use crate::generator::metadata::types::Runtime;
use crate::plugins::api::UserState;
use anyhow::Result;
use std::collections::HashMap;
use std::path::Path;

pub mod api {
    tonic::include_proto!("nirvati_app_plugin");
}

pub async fn apply_hook(
    apps_root: &Path,
    app_id: &str,
    app: &InternalAppRepresentation,
    perms: &[String],
    additional_metadata: HashMap<String, HashMap<String, serde_json::Value>>,
    user_state: UserState,
) -> Result<()> {
    context::apply_hook(apps_root, app_id, perms, additional_metadata).await;
    let runtime = &app.get_metadata().runtime;
    if let Runtime::Plugin(plugin_id) = runtime {
        runtime::apply_hook(apps_root, app_id, plugin_id, user_state).await?;
    }
    Ok(())
}

pub async fn uninstall_hook(
    apps_root: &Path,
    app_id: &str,
    perms: &[String],
    app: &InternalAppRepresentation,
) -> Result<()> {
    context::uninstall_hook(apps_root, app_id, perms).await;
    let runtime = &app.get_metadata().runtime;
    if let Runtime::Plugin(plugin_id) = runtime {
        runtime::uninstall_hook(apps_root, app_id, plugin_id).await?;
    }
    Ok(())
}

pub use custom_resource::convert_crs;
