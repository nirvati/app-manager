use std::collections::HashMap;

use anyhow::anyhow;
use tonic::{async_trait, Request, Response, Status};

use app_manager::generator::metadata::types::OutputMetadata;
use app_manager::manage::files::{read_stores_yml, write_stores_yml};
use app_manager::manage::registry::translate_all_categories;
use app_manager::parser::{load_app, process_apps};
use app_manager::{manage, repos};
use app_manager::generator::internal::ResolverCtx;

use crate::grpc::api::{
    get_app_update_request, AddStoreRequest, AppUpdate, AppUpdates, GetAppUpdateRequest,
    PreloadAppsRequest, Release, RemoveStoreRequest,
};

use super::api::download_apps_request::Reference;
use super::api::{manager_server::Manager, DownloadAppsRequest, Empty, GenerateFilesRequest};
use super::{expect_result, require_opt, ApiServer};

#[async_trait]
impl Manager for ApiServer {
    async fn preload_apps(
        &self,
        request: Request<PreloadAppsRequest>,
    ) -> Result<Response<Empty>, Status> {
        let request = request.into_inner();
        expect_result!(
            manage::init::preload_apps(
                &self.main_apps_root,
                &self.nirvati_seed,
                &request.user,
                self.instance_type
            )
            .await,
            "Failed to preload apps"
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn generate_files(
        &self,
        request: Request<GenerateFilesRequest>,
    ) -> Result<Response<Empty>, Status> {
        let request = request.into_inner();
        let user_state = require_opt!(request.user_state.clone())?;
        let apps_root = self.main_apps_root.join(&user_state.user);
        let mut resolver_ctx = ResolverCtx::new(
            apps_root.clone(),
            user_state.clone().into(),
            self.nirvati_seed.clone(),
            self.instance_type,
        );
        {
            let mut registry: Vec<OutputMetadata> = expect_result!(
                process_apps(
                    &apps_root,
                    user_state.into(),
                    &self.nirvati_seed,
                    self.instance_type,
                    &mut resolver_ctx,
                )
                .await,
                "Failed to gather app metadata!"
            )?
            .into_iter()
            .map(|app| app.into_metadata())
            .collect();
            translate_all_categories(&mut registry);
            expect_result!(
                manage::files::write_app_registry(&apps_root, &registry),
                "Failed to write app registry"
            )?;
        }
        Ok(Response::new(Empty {}))
    }

    async fn download(
        &self,
        request: Request<DownloadAppsRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let apps_root = self.main_apps_root.join(&user_state.user);
        let Some(app_ref) = req.reference else {
            return Err(Status::invalid_argument("No app reference provided"));
        };
        match app_ref {
            Reference::Store(store) => {
                if let Some(lookup) = store.store_src {
                    let mut stores_yml =
                        expect_result!(read_stores_yml(&apps_root), "Failed to read stores.yml")?;
                    let stores_yml_clone = stores_yml.clone();
                    let selected_store = stores_yml
                        .iter_mut()
                        .find(|store| store.src == lookup)
                        .ok_or_else(|| Status::not_found("Store does not exist"))?;
                    let mut previous_apps = stores_yml_clone
                        .into_iter()
                        .take_while(|store| store.src == lookup)
                        .flat_map(|store| store.apps)
                        .collect::<Vec<_>>();
                    if store.new_or_not_installed_only {
                        previous_apps.extend(user_state.installed_apps.clone());
                    }
                    expect_result!(
                        repos::download_apps_for_store(
                            &apps_root,
                            selected_store,
                            &previous_apps,
                            user_state.into(),
                        )
                        .await,
                        format!("Failed to download apps for store {:?}", lookup)
                    )?;
                    expect_result!(
                        write_stores_yml(&apps_root, &stores_yml),
                        "Failed to write stores.yml"
                    )?;
                } else {
                    expect_result!(
                        repos::download_apps(
                            &apps_root,
                            store.new_or_not_installed_only,
                            user_state.into(),
                        )
                        .await,
                        "Failed to download apps"
                    )?;
                }
            }
            Reference::App(app) => {
                // Check if app exists
                if !manage::files::app_exists(&apps_root, &app) {
                    return Err(Status::not_found("App does not exist"));
                }
                expect_result!(
                    repos::download_app(&apps_root, &app, user_state.into()).await,
                    "Failed to download app"
                )?;
            }
        }
        Ok(Response::new(Empty {}))
    }

    async fn get_updates(
        &self,
        request: Request<GetAppUpdateRequest>,
    ) -> Result<Response<AppUpdates>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let apps_root = self.main_apps_root.join(&user_state.user);
        let Some(app_ref) = req.reference else {
            return Err(Status::invalid_argument("No app reference provided"));
        };
        let stores_yml = expect_result!(read_stores_yml(&apps_root), "Failed to read stores.yml")?;
        let mut resolver_ctx = ResolverCtx::new(
            apps_root.clone(),
            user_state.clone().into(),
            self.nirvati_seed.clone(),
            self.instance_type,
        );
        let updates = expect_result!(
            match app_ref {
                get_app_update_request::Reference::Store(store_ref) => {
                    if let Some(lookup) = store_ref.store_src {
                        let store = stores_yml
                            .iter()
                            .find(|store| store.src == lookup)
                            .ok_or_else(|| Status::not_found("Store does not exist"))?;
                        repos::check_updates_for_store(
                            &apps_root,
                            store.to_owned(),
                            None,
                            &self.nirvati_seed,
                            user_state.clone().into(),
                            self.instance_type,
                            &mut resolver_ctx,
                        )
                        .await
                    } else {
                        repos::check_updates(
                            &apps_root,
                            &self.nirvati_seed,
                            user_state.clone().into(),
                            self.instance_type,
                            &mut resolver_ctx,
                        )
                        .await
                    }
                }
                get_app_update_request::Reference::App(app) => {
                    // Check if app exists
                    if !manage::files::app_exists(&apps_root, &app) {
                        return Err(Status::not_found("App does not exist"));
                    }
                    let store = expect_result!(
                        stores_yml
                            .iter()
                            .find(|store| store.apps.contains(&app))
                            .ok_or(anyhow!("Failed to get store")),
                        "Failed to get store"
                    )?;
                    repos::check_updates_for_store(
                        &apps_root,
                        store.to_owned(),
                        Some(app),
                        &self.nirvati_seed,
                        user_state.clone().into(),
                        self.instance_type,
                            &mut resolver_ctx,
                    )
                    .await
                }
            },
            "Failed to check for updates"
        )?;
        let app_updates = expect_result!(
            futures::future::try_join_all(updates.into_iter().map(|update| {
                // TODO: Make access to these more efficient
                let apps_root = apps_root.clone();
                let nirvati_seed = self.nirvati_seed.clone();
                let instance_type = self.instance_type;
                let user_state = user_state.clone();
                let mut resolver_ctx = resolver_ctx.clone();
                tokio::spawn(async move {
                    let current_metadata = expect_result!(
                        load_app(
                            &apps_root,
                            &update.id,
                            user_state.into(),
                            &nirvati_seed,
                            instance_type,
                            None,
                            &mut resolver_ctx,
                        )
                        .await,
                        format!("Failed to read metadata.yml for app {}", update.id)
                    )?
                    .into_metadata();
                    let current_version = current_metadata.version;
                    let latest_version = update.new_version;
                    // Get all releases in the release_notes until we reach the current version
                    let releases_notes = update
                        .release_notes
                        .into_iter()
                        .map_while(|(version, r)| {
                            let parsed = semver::Version::parse(&version);
                            match parsed {
                                Ok(version) => {
                                    if version == current_version {
                                        None
                                    } else {
                                        Some((
                                            version.to_string(),
                                            Release {
                                                notes: HashMap::from_iter(r.into_iter()),
                                            },
                                        ))
                                    }
                                }
                                Err(_) => {
                                    tracing::warn!("Failed to parse version {}", version);
                                    None
                                }
                            }
                        })
                        .collect::<HashMap<_, _>>();
                    Ok::<_, Status>((
                        update.id,
                        AppUpdate {
                            current: current_version.to_string(),
                            latest: latest_version,
                            releases_notes,
                        },
                    ))
                })
            },))
            .await,
            "Failed to properly parse updates"
        )?
        .into_iter()
        .collect::<Result<_, _>>()?;
        Ok(Response::new(AppUpdates {
            updates: app_updates,
        }))
    }

    async fn add_store(
        &self,
        request: Request<AddStoreRequest>,
    ) -> Result<Response<Empty>, Status> {
        let request = request.into_inner();
        let user_state = require_opt!(request.user_state.clone())?;
        let apps_root = self.main_apps_root.join(&user_state.user);
        let store_type = expect_result!(
            request.r#type.try_into(),
            "Invalid store type",
            Status::invalid_argument
        )?;
        expect_result!(
            repos::add_new_store(&apps_root, request.src, store_type, user_state.into()).await,
            "Failed to add new store!"
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn remove_store(
        &self,
        request: Request<RemoveStoreRequest>,
    ) -> Result<Response<Empty>, Status> {
        let request = request.into_inner();
        let user_state = require_opt!(request.user_state.clone())?;
        let apps_root = self.main_apps_root.join(user_state.user);
        let mut stores = expect_result!(read_stores_yml(&apps_root), "Failed to read stores.yml")?;
        stores = stores
            .into_iter()
            .filter(|store| request.store_src == store.src)
            .collect::<Vec<_>>();
        expect_result!(
            write_stores_yml(&apps_root, &stores),
            "Failed to save stores.yml"
        )?;
        Ok(Response::new(Empty {}))
    }
}
