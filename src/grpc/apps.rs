use std::collections::HashMap;

use tonic::{async_trait, Request, Response, Status};

use app_manager::generator::helm::upload_chart;
use app_manager::generator::kubernetes::generator::generate_kubernetes_config;
use app_manager::manage::files::read_plugins_json;
use app_manager::plugins::{apply_hook, uninstall_hook};

use super::api::{
    apps_server::Apps, AddDomainRequest, Empty, InstallRequest, RemoveDomainRequest,
    UninstallRequest, UserState,
};
use super::{expect_result, require_opt, ApiServer};

impl UserState {
    pub fn get_app_settings(&self) -> HashMap<String, HashMap<String, serde_yaml::Value>> {
        self.app_settings
            .iter()
            .map(|(k, v)| (k.clone(), serde_json::from_str(v).unwrap()))
            .collect()
    }

    pub fn into_app_settings(self) -> HashMap<String, HashMap<String, serde_yaml::Value>> {
        self.app_settings
            .into_iter()
            .map(|(k, v)| (k, serde_json::from_str(&v).unwrap()))
            .collect()
    }
}

#[async_trait]
impl Apps for ApiServer {
    async fn apply(&self, request: Request<InstallRequest>) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let apps_root = self.main_apps_root.join(&user_state.user);
        let app = self
            .load_app(&req.id, &user_state, req.initial_domain.clone())
            .await?;
        let plugins = app.get_plugins();
        for plugin in plugins {
            let mut current_plugins = read_plugins_json(&apps_root).await.unwrap_or_default();
            current_plugins.insert(format!("{}-{}", req.id, plugin.name), plugin.clone());
            expect_result!(
                app_manager::manage::files::write_plugins_json(&apps_root, &current_plugins).await,
                "Failed to write plugins.json"
            )?;
        }
        let app_metadata = app.get_metadata().clone();
        let version = app_metadata.version.clone();
        let app_exported_data = app_metadata.exported_data.clone().unwrap_or_default();
        let perms = app.get_metadata().has_permissions.clone();
        let kube_config = expect_result!(
            generate_kubernetes_config(app.clone(), user_state.clone().into(), &apps_root).await,
            "Failed to generate Kubernetes config"
        )?;
        // To ease migration, ignore the result of upload_chart for now
        expect_result!(
            upload_chart(
                kube_config.clone(),
                app_metadata,
                &user_state.user,
                &self.chartmuseum_url,
            )
            .await,
            "Failed to upload chart"
        )?;
        expect_result!(
            apply_hook(
                &apps_root,
                &req.id,
                &app,
                &perms,
                app_exported_data,
                user_state.clone().into()
            )
            .await,
            "Failed to run plugin hooks"
        )?;
        expect_result!(
            app_manager::kubernetes::app::install_chart(
                self.kube_client.clone(),
                &req.id,
                &user_state.user,
                version.to_string()
            )
            .await,
            "Failed to apply kubernetes config"
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn uninstall(
        &self,
        request: Request<UninstallRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let apps_root = self.main_apps_root.join(&user_state.user);
        let mut current_plugins = read_plugins_json(&apps_root).await.unwrap_or_default();
        current_plugins.retain(|k, _| !k.starts_with(&format!("{}-", req.id)));
        expect_result!(
            app_manager::manage::files::write_plugins_json(&apps_root, &current_plugins).await,
            "Failed to write plugins.json"
        )?;
        let app = self.load_app(&req.id, &user_state, None).await?;
        expect_result!(
            uninstall_hook(
                &apps_root,
                &req.id,
                &app.get_metadata().has_permissions,
                &app
            )
            .await,
            "Failed to run plugin hooks"
        )?;
        expect_result!(
            app_manager::kubernetes::app::uninstall_chart(
                self.kube_client.clone(),
                &req.id,
                &user_state.user
            )
            .await,
            "Failed to apply kubernetes config"
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn add_domain(
        &self,
        request: Request<AddDomainRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let app = self.load_app(&req.id, &user_state, None).await?;
        expect_result!(
            app_manager::manage::domains::add_app_domain(
                &self.kube_client,
                app,
                &req.domain,
                &user_state.user,
            )
            .await,
            "Failed to add ingress for domain"
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn remove_domain(
        &self,
        request: Request<RemoveDomainRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        expect_result!(
            app_manager::manage::domains::delete_app_domain(
                &self.kube_client,
                req.id,
                user_state.user,
                req.domain
            )
            .await,
            "Failed to add ingress for domain"
        )?;
        Ok(Response::new(Empty {}))
    }
}
