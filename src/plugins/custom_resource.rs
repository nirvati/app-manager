use super::api::ConvertCrRequest;
use crate::generator::internal::types::CustomResource;
use crate::manage::files::read_plugins_json;
use crate::plugins::api::UserState;
use anyhow::{bail, Result};
use serde_yaml::Value::Sequence;
use std::path::Path;
use tonic::Request;
pub async fn convert_crs(
    apps_root: &Path,
    app_id: &str,
    crs: &[CustomResource],
    user_state: UserState,
) -> Result<Vec<serde_yaml::Value>> {
    let plugins = read_plugins_json(apps_root).await?;
    let mut result: Vec<serde_yaml::Value> = vec![];
    for cr in crs {
        let plugin = plugins.get(&cr.plugin);
        if let Some(plugin) = plugin {
            let mut client = crate::plugins::api::custom_resource_plugin_client::CustomResourcePluginClient::connect(plugin.endpoint.clone()).await?;
            let response = client
                .generate_c_rs(Request::new(ConvertCrRequest {
                    app_id: app_id.to_string(),
                    resource: serde_yaml::to_string(&cr.definition)?,
                    user_state: Some(user_state.clone()),
                }))
                .await?;
            let parsed: serde_yaml::Value = serde_yaml::from_str(&response.into_inner().resources)?;
            if let Sequence(parsed) = parsed {
                result.extend(parsed);
            } else {
                result.push(parsed);
            }
        } else {
            bail!("Plugin {} not found", cr.plugin);
        }
    }
    Ok(result)
}
