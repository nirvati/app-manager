use crate::generator::internal::types::PluginType;
use crate::manage::files::read_plugins_json;
use crate::plugins::api::{InstallAppRequest, UninstallAppRequest, UserState};
use anyhow::{anyhow, bail, Result};
use std::path::Path;
use tonic::Request;

pub async fn apply_hook(
    apps_root: &Path,
    app_id: &str,
    plugin_id: &str,
    user_state: UserState,
) -> Result<()> {
    let plugins_json = read_plugins_json(apps_root).await?;
    let plugin = plugins_json
        .get(plugin_id)
        .ok_or_else(|| anyhow!("Plugin not found"))?;
    if plugin.r#type != PluginType::Runtime {
        bail!("Plugin is not a runtime plugin");
    };
    let mut client = crate::plugins::api::runtime_plugin_client::RuntimePluginClient::connect(
        plugin.endpoint.clone(),
    )
    .await?;
    let app_directory = crate::utils::tar::compress_dir(&apps_root.join(app_id)).await?;
    client
        .install_app(Request::new(InstallAppRequest {
            app_id: app_id.to_string(),
            app_directory,
            user_state: Some(user_state),
        }))
        .await?;
    Ok(())
}

pub async fn uninstall_hook(apps_root: &Path, app_id: &str, plugin_id: &str) -> Result<()> {
    let plugins_json = read_plugins_json(apps_root).await?;
    let plugin = plugins_json
        .get(plugin_id)
        .ok_or_else(|| anyhow!("Plugin not found"))?;
    if plugin.r#type != PluginType::Runtime {
        bail!("Plugin is not a runtime plugin");
    };
    let mut client = crate::plugins::api::runtime_plugin_client::RuntimePluginClient::connect(
        plugin.endpoint.clone(),
    )
    .await?;
    let app_directory = crate::utils::tar::compress_dir(&apps_root.join(app_id)).await?;
    client
        .uninstall_app(Request::new(UninstallAppRequest {
            app_id: app_id.to_string(),
            app_directory,
        }))
        .await?;
    Ok(())
}
