use crate::constants::RESERVED_NAMES;
use crate::manage::files::read_plugins_json;
use crate::plugins::api::{RegisterAppRequest, UnregisterAppRequest};
use std::collections::HashMap;
use std::path::Path;
use tonic::Request;

pub async fn apply_hook(
    apps_root: &Path,
    app_id: &str,
    perms: &[String],
    additional_metadata: HashMap<String, HashMap<String, serde_json::Value>>,
) {
    // Perms are in the format <app-id>/<perm> or just <app-id>
    let mut perms_by_app = HashMap::new();
    for perm in perms {
        let mut split = perm.split('/');
        let app_id = split.next().unwrap();
        let perm = split.next().unwrap_or("");
        if RESERVED_NAMES.contains(&app_id) {
            continue;
        }
        let perms = perms_by_app
            .entry(app_id.to_owned())
            .or_insert_with(Vec::new);
        perms.push(perm.to_owned());
    }
    let plugins = read_plugins_json(apps_root).await.unwrap_or_default();
    let plugins = plugins
        .iter()
        .filter(|(k, _)| perms_by_app.contains_key(*k));
    for (plugin_app, plugin_endpoint) in plugins {
        let metadata = additional_metadata.get(plugin_app);
        let serialized_metadata = if let Some(metadata) = metadata {
            serde_json::to_string(metadata).unwrap()
        } else {
            "{}".to_owned()
        };
        let mut client =
            match crate::plugins::api::context_plugin_client::ContextPluginClient::connect(
                plugin_endpoint.endpoint.clone(),
            )
            .await
            {
                Ok(client) => client,
                Err(err) => {
                    tracing::error!("Failed to connect to plugin {}: {:#}", plugin_app, err);
                    continue;
                }
            };
        if let Err(err) = client
            .register_app(Request::new(RegisterAppRequest {
                app_id: app_id.to_owned(),
                permissions: perms_by_app[plugin_app].clone(),
                additional_data: serialized_metadata,
            }))
            .await
        {
            tracing::error!("Failed to call plugin {}: {:#}", plugin_app, err);
            continue;
        };
    }
}

pub async fn uninstall_hook(apps_root: &Path, app_id: &str, perms: &[String]) {
    // Perms are in the format <app-id>/<perm> or just <app-id>
    let mut used_plugins = Vec::new();
    for perm in perms {
        let mut split = perm.split('/');
        let app_id = split.next().unwrap();
        if RESERVED_NAMES.contains(&app_id) {
            continue;
        }
        used_plugins.push(app_id.to_owned());
    }
    let plugins = read_plugins_json(apps_root).await.unwrap_or_default();
    let plugins = plugins.iter().filter(|(k, _)| used_plugins.contains(*k));
    for (plugin_app, plugin_endpoint) in plugins {
        let mut client =
            match crate::plugins::api::context_plugin_client::ContextPluginClient::connect(
                plugin_endpoint.endpoint.clone(),
            )
            .await
            {
                Ok(client) => client,
                Err(err) => {
                    tracing::error!("Failed to connect to plugin {}: {:#}", plugin_app, err);
                    continue;
                }
            };
        if let Err(err) = client
            .unregister_app(Request::new(UnregisterAppRequest {
                app_id: app_id.to_owned(),
            }))
            .await
        {
            tracing::error!("Failed to call plugin {}: {:#}", plugin_app, err);
            continue;
        };
    }
}
