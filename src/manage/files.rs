use std::collections::HashMap;
use std::path::Path;

use crate::generator::internal::types::PluginInfo;
use anyhow::Result;

use crate::generator::metadata::types::OutputMetadata;
use crate::repos::types::{AppStore, StoresYml};

pub fn write_app_registry(apps_root: &Path, app_registry: &[OutputMetadata]) -> Result<()> {
    let app_registry_path = apps_root.join("registry.json");
    let app_registry = serde_json::to_string_pretty(app_registry)?;
    std::fs::write(app_registry_path, app_registry)?;
    Ok(())
}

pub fn app_exists(apps_root: &Path, app_name: &str) -> bool {
    let app_dir = apps_root.join(app_name);
    app_dir.exists()
}

pub fn read_stores_yml(apps_root: &Path) -> Result<StoresYml> {
    let stores_yml_path = apps_root.join("stores.yml");
    let stores_yml: StoresYml = serde_yaml::from_str(&std::fs::read_to_string(stores_yml_path)?)?;
    Ok(stores_yml)
}

pub fn write_stores_yml(apps_root: &Path, data: &[AppStore]) -> Result<()> {
    let stores_yml_path = apps_root.join("stores.yml");
    let stores_yml = serde_yaml::to_string(&data)?;
    std::fs::write(stores_yml_path, stores_yml)?;
    Ok(())
}

pub async fn write_plugins_json(
    apps_root: &Path,
    data: &HashMap<String, PluginInfo>,
) -> Result<()> {
    let plugins_json_path = apps_root.join("plugins.json");
    let plugins_json = serde_json::to_string_pretty(&data)?;
    tokio::fs::write(plugins_json_path, plugins_json).await?;
    Ok(())
}

pub async fn read_plugins_json(apps_root: &Path) -> Result<HashMap<String, PluginInfo>> {
    let plugins_json_path = apps_root.join("plugins.json");
    let plugins_json = tokio::fs::read_to_string(plugins_json_path).await?;
    let plugins_json = serde_json::from_str(&plugins_json)?;
    Ok(plugins_json)
}
