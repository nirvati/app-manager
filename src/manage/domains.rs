use k8s_crds_traefik::IngressRoute;
use kube::api::DeleteParams;
use kube::{Api, Client};
use slugify::slugify;

use crate::generator::internal::types::InternalAppRepresentation;
use crate::generator::kubernetes::generator::ingress::get_ingress_routes;
use crate::kubernetes::apply_with_ns;

pub async fn add_app_domain(
    client: &Client,
    app: InternalAppRepresentation,
    domain: &str,
    user: &str,
) -> anyhow::Result<()> {
    let ingress = get_ingress_routes(domain.to_string(), app.get_ingress(), user);
    let ns = format!("{}-{}", user, app.get_metadata().id);
    apply_with_ns(client.clone(), &ingress, &ns).await?;
    Ok(())
}

pub async fn delete_app_domain(
    client: &Client,
    user: String,
    domain: String,
    app: String,
) -> anyhow::Result<()> {
    let dp = DeleteParams::default();
    let ns = format!("{}-{}", user, app);
    let api: Api<IngressRoute> = Api::namespaced(client.clone(), &ns);
    api.delete(&slugify!(&domain).to_string(), &dp).await?;
    api.delete(&format!("{}-https-redirect", slugify!(&domain)), &dp)
        .await?;
    Ok(())
}
