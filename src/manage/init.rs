use std::path::Path;

use crate::generator::internal::ResolverCtx;
use anyhow::Result;

use crate::generator::metadata::types::OutputMetadata;
use crate::instance_type::InstanceType;
use crate::manage::files::{write_app_registry, write_stores_yml};
use crate::manage::registry::translate_all_categories;
use crate::parser::process_apps;
use crate::plugins::api::UserState;
use crate::repos::types::AppStore;

async fn init(
    apps_root: &Path,
    user_id: &str,
    nirvati_seed: &str,
    instance_type: InstanceType,
    ctx: &mut ResolverCtx,
) {
    let mut registry: Vec<OutputMetadata> = process_apps(
        apps_root,
        UserState {
            user: user_id.to_string(),
            installed_apps: vec![],
            app_settings: Default::default(),
        },
        nirvati_seed,
        instance_type,
        ctx,
    )
    .await
    .expect("Failed to gather app metadata!")
    .into_iter()
    .map(|app| app.into_metadata())
    .collect();
    translate_all_categories(&mut registry);
    write_app_registry(apps_root, &registry).expect("Failed to write app registry!");
}

pub async fn preload_apps(
    apps_root: &Path,
    nirvati_seed: &str,
    user: &str,
    instance_type: InstanceType,
) -> Result<()> {
    let user_apps_root = apps_root.join(user);
    let user_state = UserState {
        user: user.to_string(),
        installed_apps: vec![],
        app_settings: Default::default(),
    };
    std::fs::create_dir_all(&user_apps_root)?;
    let initial_stores = [
        AppStore {
            id: uuid::Uuid::new_v4(),
            src: "https://gitlab.com/nirvati/apps/foss.git#main".to_string(),
            ..Default::default()
        },
        AppStore {
            id: uuid::Uuid::new_v4(),
            src: "https://gitlab.com/nirvati/apps/essentials.git#main".to_string(),
            ..Default::default()
        },
    ];
    write_stores_yml(&user_apps_root, &initial_stores)?;
    crate::repos::download_apps(&user_apps_root, false, user_state.clone()).await?;

    let mut resolver_ctx = ResolverCtx::new(
        user_apps_root.clone(),
        user_state.clone(),
        nirvati_seed.to_string(),
        instance_type,
    );
    init(
        &user_apps_root,
        user,
        nirvati_seed,
        instance_type,
        &mut resolver_ctx,
    )
    .await;
    Ok(())
}
