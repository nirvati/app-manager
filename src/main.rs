use std::path::Path;

use kube::Client;
use tonic::transport::Server;

use app_manager::instance_type::InstanceType;
use app_manager::manage::init::preload_apps;

use crate::grpc::api::apps_server::AppsServer;
use crate::grpc::api::manager_server::ManagerServer;
use crate::grpc::api::setup_server::SetupServer;

pub mod grpc;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    #[cfg(feature = "dotenvy")]
    dotenvy::dotenv().expect("Failed to load .env file");
    // Take Nirvati path and from env
    let apps_path = std::env::var("APPS_PATH").expect("APPS_PATH not set");
    let nirvati_seed = std::env::var("NIRVATI_SEED").expect("NIRVATI_SEED not set");
    let instance_type = std::env::var("INSTANCE_TYPE").expect("INSTANCE_TYPE not set");
    let chartmuseum_url = std::env::var("CHARTMUSEUM_URL")
        .unwrap_or("http://main.admin-chartmuseum.svc.cluster.local:8080".to_string());
    let instance_type: InstanceType = instance_type
        .parse()
        .expect("Failed to parse instance type");

    let apps_root = Path::new(&apps_path);
    // If apps_dir contains no registry.json, download apps
    if !apps_root
        .read_dir()
        .unwrap()
        .any(|file| file.unwrap().file_name() == ".is_initialized")
    {
        preload_apps(apps_root, &nirvati_seed, "admin", instance_type)
            .await
            .expect("Failed to preload apps for admin");
        std::fs::File::create(apps_root.join(".is_initialized"))
            .expect("Failed to create .is_initialized file");
    }

    let api_server = grpc::ApiServer {
        main_apps_root: apps_path.clone().into(),
        kube_client: Client::try_default()
            .await
            .expect("Failed to create kube client"),
        nirvati_seed: nirvati_seed.clone(),
        instance_type,
        chartmuseum_url,
    };

    let bind_address = std::env::var("BIND_ADDRESS").unwrap_or("[::]:8080".to_string());
    tracing::info!("Starting server on {}", bind_address);
    let address = bind_address.parse().expect("Failed to parse bind address");
    let api_server_clone = api_server.clone();
    let server_1 = tokio::spawn(async move {
        Server::builder()
            .add_service(AppsServer::new(api_server_clone.clone()))
            .add_service(ManagerServer::new(api_server_clone.clone()))
            .add_service(SetupServer::new(api_server_clone))
            .serve(address)
            .await
    });

    let public_api_bind_address =
        std::env::var("PUBLIC_API_BIND_ADDRESS").unwrap_or("[::]:8081".to_string());
    tracing::info!("Starting public server on {}", public_api_bind_address);
    let public_address = public_api_bind_address
        .parse()
        .expect("Failed to parse public bind address");
    let server_2 = tokio::spawn(async move {
        Server::builder()
            .add_service(grpc::public::defs::apps_public_server::AppsPublicServer::new(api_server))
            .serve(public_address)
            .await
    });
    let (server1_res, server2_res) =
        tokio::try_join!(server_1, server_2).expect("Failed to start servers");
    server1_res.expect("Failed to start private server");
    server2_res.expect("Failed to start public server");
}
