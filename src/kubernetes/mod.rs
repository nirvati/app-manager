use anyhow::Result;
use k8s_openapi::NamespaceResourceScope;
use kube::api::{Patch, PatchParams};
use kube::{Api, Client, Resource, ResourceExt};

pub mod app;

pub async fn apply_with_ns<ResourceType>(
    client: Client,
    resources: &[ResourceType],
    namespace: &str,
) -> Result<()>
where
    <ResourceType as Resource>::DynamicType: Default,
    ResourceType: Resource<Scope = NamespaceResourceScope>
        + Clone
        + serde::de::DeserializeOwned
        + std::fmt::Debug
        + serde::Serialize,
{
    let ssapply = PatchParams::apply("nirvati").force();
    let api: Api<ResourceType> = Api::namespaced(client, namespace);
    for resource in resources {
        let name = resource.name_any();
        api.patch(&name, &ssapply, &Patch::Apply(resource)).await?;
    }
    Ok(())
}
