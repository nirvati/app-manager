use anyhow::Result;
use k8s_crds_helm_controller::HelmChartSpec;
use k8s_openapi::api::core::v1::Namespace;
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use kube::Client;

use crate::kubernetes::apply_with_ns;

pub async fn install_chart(client: Client, app: &str, user: &str, version: String) -> Result<()> {
    let chart = k8s_crds_helm_controller::HelmChart {
        metadata: ObjectMeta {
            name: Some(app.to_string()),
            namespace: Some(user.to_string()),
            ..Default::default()
        },
        spec: HelmChartSpec {
            repo: Some(format!(
                "http://main.{}-chartmuseum.svc.cluster.local:8080",
                user
            )),
            chart: Some(app.to_string()),
            version: Some(version.to_string()),
            create_namespace: Some(true),
            target_namespace: Some(format!("{}-{}", user, app)),
            ..Default::default()
        },
    };
    apply_with_ns(client.clone(), &[chart], user).await?;
    // Wait for the namespace to be created, but at most 20 seconds
    let api: kube::Api<Namespace> = kube::Api::all(client);
    let mut i = 0;
    while api.get(&format!("{}-{}", user, app)).await.is_err() && i < 20 {
        tokio::time::sleep(std::time::Duration::from_secs(1)).await;
        i += 1;
    }
    Ok(())
}

pub async fn uninstall_chart(client: Client, app: &str, user: &str) -> Result<()> {
    let api: kube::Api<k8s_crds_helm_controller::HelmChart> =
        kube::Api::namespaced(client.clone(), user);
    api.delete(app, &Default::default()).await?;
    let api: kube::Api<Namespace> = kube::Api::all(client);
    api.delete(&format!("{}-{}", user, app), &Default::default())
        .await?;
    Ok(())
}
