pub mod tar;

use std::collections::BTreeMap;
use std::fmt::{Debug, Display, Formatter};
use std::{fs, path::Path};

use anyhow::{anyhow, Result};
use hmac_sha256::HMAC;
use lazy_static::lazy_static;
use regex::Regex;
use serde::{Deserialize, Serialize};

lazy_static! {
    // A $ in front is also matched, if it is present, it is treated as an escape => ignored
    static ref ENV_VAR_REGEX: Regex = Regex::new(r"\$?(\$\{.*?}+|\$[A-Za-z1-9_]+)").unwrap();
}

// A helper for skipping deserialization of values that default to false
#[inline]
pub fn is_false(v: &bool) -> bool {
    !*v
}

#[inline]
pub fn true_default() -> bool {
    true
}

#[inline]
pub fn false_default() -> bool {
    false
}

#[derive(Serialize, Deserialize, Clone, Default, PartialEq, Eq)]
pub struct MultiLanguageItem(pub BTreeMap<String, String>);

impl Display for MultiLanguageItem {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let english_name = self.0.get("en").unwrap_or_else(|| {
            self.0
                .iter()
                .find(|(key, _)| key.starts_with("en"))
                .map(|(_, value)| value)
                .unwrap_or_else(|| self.0.iter().next().map(|(_, value)| value).unwrap())
        });
        f.write_str(english_name)
    }
}

impl Debug for MultiLanguageItem {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// A type that can be serialized into a string, but can also be various other types
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
#[serde(untagged)]
pub enum StringLike {
    String(String),
    Int(i64),
    Bool(bool),
    Float(f64),
}

impl From<StringLike> for String {
    fn from(s: StringLike) -> Self {
        match s {
            StringLike::String(s) => s,
            StringLike::Int(i) => i.to_string(),
            StringLike::Bool(b) => b.to_string(),
            StringLike::Float(f) => f.to_string(),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
#[serde(untagged)]
pub enum StringOrNumber {
    String(String),
    Int(i64),
    Float(f64),
}

// Check recursively if a dir contains any symlinks
pub fn has_symlinks(dir: &Path) -> bool {
    if !dir.is_dir() {
        return false;
    }
    for entry in fs::read_dir(dir).unwrap() {
        let entry = entry.unwrap();
        let path = entry.path();
        if path.is_dir() {
            has_symlinks(&path);
        } else if path.is_symlink() {
            return true;
        }
    }
    false
}

pub fn serde_yaml_to_json(yaml: serde_yaml::Value) -> Result<serde_json::Value> {
    match yaml {
        serde_yaml::Value::Null => Ok(serde_json::Value::Null),
        serde_yaml::Value::Bool(b) => Ok(serde_json::Value::Bool(b)),
        serde_yaml::Value::Number(n) => {
            if let Some(i) = n.as_i64() {
                Ok(serde_json::Value::Number(serde_json::Number::from(i)))
            } else if let Some(u) = n.as_u64() {
                Ok(serde_json::Value::Number(serde_json::Number::from(u)))
            } else if let Some(f) = n.as_f64() {
                Ok(serde_json::Value::Number(
                    serde_json::Number::from_f64(f).unwrap(),
                ))
            } else {
                Err(anyhow!("Failed to convert number"))
            }
        }
        serde_yaml::Value::String(s) => Ok(serde_json::Value::String(s)),
        serde_yaml::Value::Sequence(s) => {
            let mut out = Vec::new();
            for item in s {
                out.push(serde_yaml_to_json(item)?);
            }
            Ok(serde_json::Value::Array(out))
        }
        serde_yaml::Value::Mapping(m) => {
            let mut out = serde_json::Map::new();
            for (key, value) in m {
                let key = match key {
                    serde_yaml::Value::String(s) => s,
                    serde_yaml::Value::Number(num) => num.to_string(),
                    serde_yaml::Value::Bool(b) => b.to_string(),
                    _ => return Err(anyhow!("Non-string keys not supported")),
                };
                out.insert(key, serde_yaml_to_json(value)?);
            }
            Ok(serde_json::Value::Object(out))
        }
        serde_yaml::Value::Tagged { .. } => Err(anyhow!("Tagged values not supported")),
    }
}

/// Splits a string into a vector of substrings, where each substring is either a quoted string or a non-whitespace string.
/// Also handles escaped quotes. Removes the quotes from the output, unless they are escaped. If they are escaped, the escape character is removed.
///
/// # Arguments
///
/// * `input` - A string slice that holds the input string to be split.
///
/// # Returns
///
/// A vector of strings, where each string is a substring of the input string.
///
/// # Examples
///
/// ```
/// use regex::Regex;
/// use app_manager::utils::split_with_quotes;
///
/// let input = r#"hello "world" 'how are you'"#;
/// let expected_output = vec!["hello".to_string(), "world".to_string(), "how are you".to_string()];
/// let output = split_with_quotes(input);
///
/// assert_eq!(output, expected_output);
/// ```
pub fn split_with_quotes(input: &str) -> Vec<String> {
    let mut output: Vec<String> = Vec::new();
    let mut current_string = String::new();
    let mut in_quotes = false;
    let mut quote_char = ' ';
    let mut escaped = false;
    for c in input.chars() {
        if escaped {
            current_string.push(c);
            escaped = false;
            continue;
        }
        if c == '\\' {
            escaped = true;
            continue;
        }
        if c == '"' || c == '\'' {
            if in_quotes && c == quote_char {
                in_quotes = false;
                quote_char = ' ';
            } else if !in_quotes {
                in_quotes = true;
                quote_char = c;
            } else if in_quotes && c != quote_char {
                current_string.push(c);
                continue;
            }
            continue;
        }
        if c.is_whitespace() && !in_quotes {
            if !current_string.is_empty() {
                output.push(current_string);
                current_string = String::new();
            }
            continue;
        }
        current_string.push(c);
    }
    if !current_string.is_empty() {
        output.push(current_string);
    }
    output
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_is_false() {
        assert!(is_false(&false));
        assert!(!is_false(&true));
    }

    #[test]
    fn test_stringlike() {
        assert_eq!(
            Into::<String>::into(StringLike::String("Hello world".to_string())),
            "Hello world".to_string()
        );
        assert_eq!(
            Into::<String>::into(StringLike::Int(100)),
            "100".to_string()
        );
        assert_eq!(
            Into::<String>::into(StringLike::Bool(true)),
            "true".to_string()
        );
        assert_eq!(
            Into::<String>::into(StringLike::Float(1.23)),
            "1.23".to_string()
        );
    }

    #[test]
    fn test_split_with_quotes() {
        let input = r#"hello "world" 'how are you'"#;
        let expected_output = vec![
            "hello".to_string(),
            "world".to_string(),
            "how are you".to_string(),
        ];
        let output = split_with_quotes(input);
        assert_eq!(output, expected_output);
    }

    #[test]
    fn test_split_with_quotes_escaped_quotes() {
        let input = r#"hello "world \"how are you\"" goodbye"#;
        let expected_output = vec![
            "hello".to_string(),
            "world \"how are you\"".to_string(),
            "goodbye".to_string(),
        ];
        let output = split_with_quotes(input);
        assert_eq!(output, expected_output);
    }

    #[test]
    fn test_split_with_quotes_empty() {
        let input = "";
        let expected_output: Vec<String> = Vec::new();
        let output = split_with_quotes(input);
        assert_eq!(output, expected_output);
    }

    #[test]
    fn test_split_with_quotes_single() {
        let input = "hello";
        let expected_output = vec!["hello".to_string()];
        let output = split_with_quotes(input);
        assert_eq!(output, expected_output);
    }

    #[test]
    fn test_split_with_quotes_multiple_spaces() {
        let input = "hello    world";
        let expected_output = vec!["hello".to_string(), "world".to_string()];
        let output = split_with_quotes(input);
        assert_eq!(output, expected_output);
    }

    #[test]
    fn test_split_with_quotes_nested_quotes() {
        let input = r#"hello "world 'how are you'" goodbye"#;
        let expected_output = vec![
            "hello".to_string(),
            "world 'how are you'".to_string(),
            "goodbye".to_string(),
        ];
        let output = split_with_quotes(input);
        assert_eq!(output, expected_output);
    }

    #[test]
    fn test_si_to_bytes() {
        assert_eq!(si_to_bytes("1").unwrap(), 1);
        assert_eq!(si_to_bytes("1K").unwrap(), 1000);
        assert_eq!(si_to_bytes("1Ki").unwrap(), 1024);
        assert_eq!(si_to_bytes("1M").unwrap(), 1000000);
        assert_eq!(si_to_bytes("1Mi").unwrap(), 1048576);
        assert_eq!(si_to_bytes("1G").unwrap(), 1000000000);
        assert_eq!(si_to_bytes("1Gi").unwrap(), 1073741824);
        assert_eq!(si_to_bytes("1T").unwrap(), 1000000000000);
        assert_eq!(si_to_bytes("1Ti").unwrap(), 1099511627776);
        assert_eq!(si_to_bytes("1P").unwrap(), 1000000000000000);
        assert_eq!(si_to_bytes("1Pi").unwrap(), 1125899906842624);
        assert_eq!(si_to_bytes("1E").unwrap(), 1000000000000000000);
        assert_eq!(si_to_bytes("1Ei").unwrap(), 1152921504606846976);
    }

    #[test]
    fn test_si_to_bytes_invalid() {
        assert!(si_to_bytes("1Z").is_err());
        assert!(si_to_bytes("1Zi").is_err());
        assert!(si_to_bytes("1K2i").is_err());
    }
}

pub fn derive_entropy(identifier: &str, nirvati_seed: &str) -> String {
    let mut hasher = HMAC::new(nirvati_seed);
    hasher.update(identifier.as_bytes());
    let result = hasher.finalize();
    hex::encode(result)
}

pub fn si_to_bytes(input: &str) -> Result<u64, std::io::Error> {
    let mut num = String::new();
    let mut suffix = String::new();
    for c in input.chars() {
        if c.is_numeric() {
            if !suffix.is_empty() {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::InvalidInput,
                    "Invalid size input",
                ));
            }
            num.push(c);
        } else {
            suffix.push(c);
        }
    }
    let num = num.parse::<u64>().unwrap();
    Ok(match suffix.as_str() {
        "" => num,
        "Ki" => num * 1024,
        "Mi" => num * 1024 * 1024,
        "Gi" => num * 1024 * 1024 * 1024,
        "Ti" => num * 1024 * 1024 * 1024 * 1024,
        "Pi" => num * 1024 * 1024 * 1024 * 1024 * 1024,
        "Ei" => num * 1024 * 1024 * 1024 * 1024 * 1024 * 1024,
        "K" => num * 1000,
        "M" => num * 1000 * 1000,
        "G" => num * 1000 * 1000 * 1000,
        "T" => num * 1000 * 1000 * 1000 * 1000,
        "P" => num * 1000 * 1000 * 1000 * 1000 * 1000,
        "E" => num * 1000 * 1000 * 1000 * 1000 * 1000 * 1000,
        _ => {
            return Err(std::io::Error::new(
                std::io::ErrorKind::InvalidInput,
                "Invalid size input",
            ))
        }
    })
}
