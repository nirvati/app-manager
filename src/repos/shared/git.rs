use std::collections::BTreeMap;
use std::path::Path;

use anyhow::{anyhow, Result};
use gix::bstr::BStr;
use gix::{oid, prepare_clone, ObjectId};
use gix_diff::tree::recorder::{Change, Location};
use gix_object::{Find, TreeRefIter};
use gix_refspec::parse::Operation;

// TODO: Reimplement this for gix
fn repo_checkout_branch(repo: &Path, branch: &str) -> Result<()> {
    let repo = git2::Repository::open(repo)?;
    let mut checkout_builder = git2::build::CheckoutBuilder::new();
    checkout_builder.force();
    repo.checkout_head(Some(&mut checkout_builder))?;
    let branch_ref = format!("refs/remotes/origin/{}", branch);
    let branch_ref = repo.find_reference(&branch_ref)?;
    let branch_commit = branch_ref.peel_to_commit()?;
    repo.checkout_tree(&branch_commit.into_object(), Some(&mut checkout_builder))?;
    repo.set_head(branch_ref.name().unwrap())?;
    Ok(())
}

pub fn clone(repo_url: gix::Url, branch: &str, target: &Path) -> Result<()> {
    tracing::debug!("Cloning {repo_url:?} into {target:?}...");
    let prepare_clone = prepare_clone(repo_url, target)?;
    let rspec = format!("refs/heads/{}", branch);
    let target_branch_refspec = gix_refspec::parse(BStr::new(&rspec), Operation::Fetch)?.to_owned();
    let (mut prepare_checkout, _) = prepare_clone
        .with_fetch_options(gix::remote::ref_map::Options {
            extra_refspecs: vec![target_branch_refspec],
            ..Default::default()
        })
        .fetch_then_checkout(gix::progress::Discard, &gix::interrupt::IS_INTERRUPTED)?;
    let _ =
        prepare_checkout.main_worktree(gix::progress::Discard, &gix::interrupt::IS_INTERRUPTED)?;
    repo_checkout_branch(target, branch)?;
    Ok(())
}

/// Gets the currently checked out commit from a repo path
pub fn get_commit(repo_path: &Path) -> Result<String> {
    let repo = gix::discover(repo_path)?;
    let id = repo.head_commit()?.id().to_string();
    Ok(id)
}

fn locate_tree_by_commit<'a>(
    db: &gix::OdbHandle,
    commit: &oid,
    buf: &'a mut Vec<u8>,
) -> Result<TreeRefIter<'a>, gix_object::find::Error> {
    let tree_id = db
        .try_find(commit, buf)?
        .ok_or_else(|| anyhow!("start commit {commit:?} to be present"))?
        .decode()?
        .into_commit()
        .expect("id is actually a commit")
        .tree();

    Ok(db
        .try_find(&tree_id, buf)?
        .expect("main tree present")
        .try_into_tree_iter()
        .expect("id to be a tree"))
}

pub fn diff_commits(
    db: &gix::OdbHandle,
    lhs: impl Into<Option<ObjectId>>,
    rhs: &oid,
    location: Option<Location>,
) -> Result<Vec<Change>> {
    let mut buf = Vec::new();
    let lhs_tree = lhs
        .into()
        .and_then(|lhs| locate_tree_by_commit(db, &lhs, &mut buf).ok());
    let mut buf2 = Vec::new();
    let rhs_tree = locate_tree_by_commit(db, rhs, &mut buf2)
        .map_err(|err| anyhow!("Failed to locate rhs tree: {:?}", err))?;
    let mut recorder = gix_diff::tree::Recorder::default().track_location(location);
    gix_diff::tree::Changes::from(lhs_tree)
        .needed_to_obtain(
            rhs_tree,
            gix_diff::tree::State::default(),
            db,
            &mut recorder,
        )
        .map_err(|err| anyhow!("Failed to diff trees: {:?}", err))?;
    Ok(recorder.records)
}

pub fn get_latest_commit_for_apps(
    repo_path: &Path,
    app_subdir: &str,
    apps: &[String],
) -> Result<BTreeMap<String, String>> {
    let mut latest_commits = BTreeMap::new();
    let repo = gix::discover(repo_path)?;
    let revwalk = repo.rev_walk([repo.head_id()?]).all()?;
    let mut initial_commit_id = String::new();
    for commit in revwalk {
        let commit = commit?;
        // Ignore merge commits (2+ parents) because that's what 'git whatchanged' does.
        // Ignore commit with 0 parents (initial commit) because there's nothing to diff against
        let parents = commit.parent_ids().collect::<Vec<_>>();
        if parents.len() == 1 {
            let prev_commit = parents.into_iter().next().unwrap().detach();
            let diff = diff_commits(
                &repo.objects,
                Some(prev_commit),
                &commit.id,
                Some(Location::Path),
            )?;
            for change in diff {
                let path = match change {
                    Change::Addition { path, .. } => path.to_string(),
                    Change::Deletion { path, .. } => path.to_string(),
                    Change::Modification { path, .. } => path.to_string(),
                };
                if path.starts_with(app_subdir) {
                    let Some(app_name) = path.split('/').nth(1) else {
                        continue;
                    };
                    if apps.contains(&app_name.to_string())
                        && !latest_commits.contains_key(app_name)
                    {
                        latest_commits.insert(app_name.to_string(), commit.id().to_string());
                    }
                }
            }
        } else if parents.is_empty() {
            initial_commit_id = commit.id.to_string();
        }
    }
    for app in apps {
        if !latest_commits.contains_key(app) {
            latest_commits.insert(app.clone(), initial_commit_id.clone());
        }
    }
    Ok(latest_commits)
}
