use std::path::Path;

use anyhow::{anyhow, Result};
use crate::generator::internal::ResolverCtx;

use crate::instance_type::InstanceType;
use crate::manage::files::{read_stores_yml, write_stores_yml};
use crate::plugins::api::UserState;
use crate::repos::shared::git;
use crate::repos::types::{AppStore, AppUpdateInfo, StoreType};

mod nirvati;
mod plugin;
mod shared;
pub mod types;

/// Download the latest version of the app, and possibly update its data
pub async fn download_app(apps_root: &Path, app: &str, user_state: UserState) -> Result<()> {
    let mut stores = read_stores_yml(apps_root)?;
    let app_src = stores
        .iter_mut()
        .find(|store| store.apps.contains(&app.to_string()))
        .ok_or_else(|| {
            anyhow!("App not found in any store - Make sure it has been downloaded before.")
        })?;
    match app_src.r#type {
        StoreType::Nirvati => nirvati::download_app(apps_root, app, app_src).map(|_| ())?,
        StoreType::Plugin => plugin::download_app(apps_root, app, app_src, user_state)
            .await
            .map(|_| ())?,
    };
    write_stores_yml(apps_root, &stores)?;
    Ok(())
}
pub async fn download_apps_for_store(
    apps_root: &Path,
    store: &mut AppStore,
    skip_apps: &[String],
    user_state: UserState,
) -> Result<Vec<String>> {
    match store.r#type {
        StoreType::Nirvati => nirvati::download_apps_for_store(apps_root, store, skip_apps),
        StoreType::Plugin => {
            plugin::download_apps_for_store(apps_root, store, skip_apps, user_state).await
        }
    }
}

pub async fn add_new_store(
    apps_root: &Path,
    src: String,
    type_: StoreType,
    user_state: UserState,
) -> Result<Vec<String>> {
    let mut stores = read_stores_yml(apps_root)?;
    let all_apps = stores
        .iter()
        .flat_map(|store| store.apps.clone())
        .collect::<Vec<_>>();
    let mut new_store = AppStore {
        id: uuid::Uuid::new_v4(),
        src,
        r#type: type_,
        ..Default::default()
    };
    let result = download_apps_for_store(apps_root, &mut new_store, &all_apps, user_state).await?;
    stores.push(new_store);
    write_stores_yml(apps_root, &stores)?;
    Ok(result)
}

pub async fn download_apps(
    apps_root: &Path,
    new_and_not_installed_only: bool,
    user_state: UserState,
) -> Result<()> {
    let mut stores = read_stores_yml(apps_root)?;
    let mut all_apps_from_stores: Vec<String> = vec![];
    if new_and_not_installed_only {
        all_apps_from_stores.extend(user_state.installed_apps.clone());
    }
    // For each AppSrc, clone the repo into a tempdir
    for store in stores.iter_mut() {
        let store_apps =
            download_apps_for_store(apps_root, store, &all_apps_from_stores, user_state.clone())
                .await?;
        all_apps_from_stores.extend(store_apps);
    }

    write_stores_yml(apps_root, &stores)?;

    Ok(())
}

pub async fn check_updates_for_store(
    apps_root: &Path,
    store: AppStore,
    app: Option<String>,
    nirvati_seed: &str,
    user_state: UserState,
    instance_type: InstanceType,
    ctx: &mut ResolverCtx,
) -> Result<Vec<AppUpdateInfo>> {
    match store.r#type {
        StoreType::Nirvati => {
            nirvati::check_updates_for_store(store, app, nirvati_seed, user_state, instance_type, ctx)
                .await
        }
        StoreType::Plugin => {
            plugin::check_updates_for_store(apps_root, store, app, user_state).await
        }
    }
}

pub async fn check_updates(
    apps_root: &Path,
    nirvati_seed: &str,
    user_state: UserState,
    instance_type: InstanceType,
    ctx: &mut ResolverCtx,
) -> Result<Vec<AppUpdateInfo>> {
    let mut updatable_apps = vec![];

    let stores = read_stores_yml(apps_root)?;

    for store in stores {
        let updates = check_updates_for_store(
            apps_root,
            store,
            None,
            nirvati_seed,
            user_state.clone(),
            instance_type,
            ctx,
        )
        .await?;
        updatable_apps.extend(updates);
    }

    Ok(updatable_apps)
}
