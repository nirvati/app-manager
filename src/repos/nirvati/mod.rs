use std::collections::HashMap;
use std::fs::File;
use std::path::{Path, PathBuf};
use std::str::FromStr;

use anyhow::{anyhow, bail, Result};
use semver::Version;
use serde::{Deserialize, Serialize};
use tempdir::TempDir;

use crate::constants::{MINIMUM_COMPATIBLE_APP_MANAGER, RESERVED_NAMES};
use crate::generator::internal::ResolverCtx;
use crate::instance_type::InstanceType;
use crate::parser::load_app;
use crate::plugins::api::UserState;
use crate::repos::nirvati::files::load_app_store_yml;
use crate::repos::nirvati::types::{AppStoreV1, AppStoreYml};
use crate::repos::types::{AppStore, AppUpdateInfo, StoreType};
use crate::utils::{has_symlinks, MultiLanguageItem};

use super::git;

mod files;
mod types;

#[derive(Serialize, Deserialize)]
struct ProviderData {
    pub apps: HashMap<String, String>,
    pub commit: String,
}

impl From<serde_yaml::Value> for ProviderData {
    fn from(value: serde_yaml::Value) -> Self {
        serde_yaml::from_value(value).unwrap_or(Self {
            apps: HashMap::new(),
            commit: String::new(),
        })
    }
}

struct RepoSrc {
    repo_url: gix::url::Url,
    branch: String,
}

impl FromStr for RepoSrc {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let (repo, branch) = s.split_once('#').ok_or(anyhow!("Failed to parse repo"))?;
        let repo_url = gix::url::parse(repo.as_ref())?;
        Ok(RepoSrc {
            repo_url,
            branch: branch.to_string(),
        })
    }
}

fn get_subdir(app_store: &AppStoreV1) -> Option<String> {
    let mut subdir = None;
    if app_store.content.contains_key(env!("CARGO_PKG_VERSION")) {
        subdir = Some(
            app_store
                .content
                .get(env!("CARGO_PKG_VERSION"))
                .unwrap()
                .clone(),
        );
    } else if app_store
        .content
        .contains_key(&("v".to_owned() + env!("CARGO_PKG_VERSION")))
    {
        subdir = Some(
            app_store
                .content
                .get(&("v".to_owned() + env!("CARGO_PKG_VERSION")))
                .unwrap()
                .clone(),
        );
    } else {
        let current_version = Version::parse(env!("CARGO_PKG_VERSION")).unwrap();
        let minimum_app_manager = Version::parse(MINIMUM_COMPATIBLE_APP_MANAGER).unwrap();
        // The semver of the latest found version so we can compare it to find a later one
        let mut found_version: Option<Version> = None;
        for (key, value) in app_store.content.iter() {
            let key = key.strip_prefix('v').unwrap_or(key);
            let key = Version::parse(key).unwrap();
            if key >= minimum_app_manager
                && key <= current_version
                && (found_version.is_none() || key > found_version.as_ref().unwrap().clone())
            {
                found_version = Some(key);
                subdir = Some(value.clone());
            }
        }
    }
    subdir
}

// Returns apps downloaded from this store
pub fn download_apps_for_store(
    apps_root: &Path,
    store: &mut AppStore,
    skip_apps: &[String],
) -> Result<Vec<String>> {
    let mut store_apps = vec![];
    let tmp_dir = TempDir::new("nirvati_app")?;
    let mut provider_data: ProviderData = store.provider_data.clone().into();
    let parsed_repo: RepoSrc = store.src.parse()?;
    git::clone(parsed_repo.repo_url, &parsed_repo.branch, tmp_dir.path())?;
    // Read the app-store.yml, and match the store_version
    let app_store_yml = tmp_dir.path().join("app-store.yml");
    let app_store_yml = load_app_store_yml(&app_store_yml, &store.src)?;
    match app_store_yml {
        AppStoreYml::AppStoreV1(app_store) => {
            let mut globals = None;
            let globals_yml = tmp_dir.path().join("globals.yml");
            if globals_yml.exists() {
                let Ok(globals_yml_file) = File::open(&globals_yml) else {
                    bail!("Failed to load globals.yml in {}", &store.src);
                };
                let globals_yml_parsed =
                    serde_yaml::from_reader::<_, serde_json::Value>(globals_yml_file);
                if let Err(err) = globals_yml_parsed {
                    bail!("Invalid globals.yml in {}: {:#?}", &store.src, err);
                }
                let new_path = apps_root.join(format!("{}-globals.yml", &store.id));
                std::fs::copy(&globals_yml, &new_path)?;
                globals = Some(new_path);
            }
            let Some(subdir) = get_subdir(&app_store) else {
                bail!("No compatible version found for {}", &store.src);
            };
            let mut out_app_store = AppStore {
                id: store.id,
                src: store.src.clone(),
                name: MultiLanguageItem(app_store.name.into()),
                tagline: MultiLanguageItem(app_store.tagline.into()),
                description: MultiLanguageItem(app_store.description.into()),
                icon: app_store.icon,
                developers: app_store.developers,
                license: app_store.license,
                r#type: StoreType::Nirvati,
                // This will always be replaced later
                provider_data: serde_yaml::Value::Null,
                apps: store.apps.clone(),
            };
            let subdir_path = Path::new(&subdir);
            // Copy all dirs from the subdir to the apps dir
            // Overwrite any existing files
            // Skip apps that are already in installed_apps
            for entry in std::fs::read_dir(tmp_dir.path().join(subdir_path))? {
                let entry = entry?;
                if !entry.path().is_dir() {
                    continue;
                }
                let app_id = entry
                    .file_name()
                    .to_str()
                    .unwrap()
                    .to_string()
                    .to_lowercase()
                    .replace('_', "-");
                if !app_id.is_ascii() {
                    tracing::error!(
                        "App store \"{}\" tries to install an app with the non-ASCII name \"{}\",",
                        out_app_store.name,
                        app_id
                    );
                    continue;
                }
                if skip_apps.contains(&app_id) {
                    continue;
                }
                if RESERVED_NAMES.contains(&app_id.as_str()) {
                    tracing::error!(
                        "App store \"{}\" tries to install an app with the reserved name \"{}\",",
                        out_app_store.name,
                        app_id
                    );
                    continue;
                }
                if has_symlinks(&entry.path()) {
                    tracing::warn!(
                        "App store \"{}\" tries to install app \"{}\", but it contains symlinks.",
                        out_app_store.id,
                        app_id
                    );
                    continue;
                }
                fs_extra::dir::copy(
                    entry.path(),
                    apps_root,
                    &fs_extra::dir::CopyOptions {
                        overwrite: true,
                        skip_exist: true,
                        buffer_size: 64000,
                        copy_inside: true,
                        depth: 0,
                        content_only: false,
                    },
                )?;
                if let Some(globals) = &globals {
                    let new_path = apps_root.join(app_id.clone()).join("globals.yml");
                    // Check if new_path exists (not following symlinks)
                    if std::fs::symlink_metadata(&new_path).is_ok() {
                        std::fs::remove_file(&new_path)?;
                    }
                    let globals_relative_path =
                        PathBuf::from("..").join(globals.file_name().unwrap());
                    // Create a symlink
                    #[cfg(unix)]
                    std::os::unix::fs::symlink(globals_relative_path, &new_path)?;
                    #[cfg(windows)]
                    std::os::windows::fs::symlink_file(globals, &new_path)?;
                }
                store_apps.push(app_id);
            }
            let new_apps = git::get_latest_commit_for_apps(tmp_dir.path(), &subdir, &store_apps)?;
            for (app, commit) in new_apps {
                provider_data.apps.insert(app, commit);
            }
            out_app_store.provider_data = serde_yaml::to_value(&provider_data)?;
            out_app_store.apps = provider_data.apps.keys().cloned().collect();
            *store = out_app_store;
        }
    }
    Ok(store_apps)
}

pub fn download_app(apps_root: &Path, app: &str, store: &mut AppStore) -> Result<()> {
    let tmp_dir = TempDir::new("nirvati")?;
    let parsed_repo: RepoSrc = store.src.parse()?;
    let mut provider_data: ProviderData = store.provider_data.clone().into();
    git::clone(parsed_repo.repo_url, &parsed_repo.branch, tmp_dir.path())?;
    let app_store_yml = tmp_dir.path().join("app-store.yml");
    let app_store_yml = File::open(app_store_yml)
        .ok()
        .ok_or_else(|| anyhow!("No app-store.yml found in {}", store.src))?;
    let app_store =
        serde_yaml::from_reader::<File, serde_yaml::Value>(app_store_yml).or_else(|error| {
            bail!(
                "Failed to load app-store.yml in {}: {:#?}",
                store.src,
                error
            )
        })?;
    let app_store_version = app_store.get("store_version");
    if app_store_version.is_none() || !app_store_version.unwrap().is_u64() {
        bail!("App store version not defined.");
    }
    let app_store_version = app_store_version.unwrap().as_u64().unwrap();
    match app_store_version {
        1 => {
            let app_store = serde_yaml::from_value::<AppStoreV1>(app_store);
            let Ok(app_store) = app_store else {
                bail!("Failed to load app-store.yml in {}", store.src);
            };
            let Some(subdir) = get_subdir(&app_store) else {
                bail!("No compatible version found for {}", store.src);
            };
            // Check if app exists in store
            let app_dir = tmp_dir.path().join(&subdir).join(app);
            if !app_dir.exists() {
                bail!("App {} not present in {} anymore", app, store.src);
            }

            // Overwrite app
            let nirvati_app_dir = apps_root.join(app);
            if nirvati_app_dir.exists() {
                // Remove all contents, except for globals.yml
                for entry in std::fs::read_dir(&nirvati_app_dir)? {
                    let entry = entry?;
                    if entry.file_name() == "globals.yml" {
                        continue;
                    }
                    if entry.file_type()?.is_dir() {
                        std::fs::remove_dir_all(entry.path())?;
                    } else {
                        std::fs::remove_file(entry.path())?;
                    }
                }
            }
            std::fs::create_dir_all(&nirvati_app_dir)?;

            fs_extra::dir::copy(
                &app_dir,
                apps_root,
                &fs_extra::dir::CopyOptions {
                    overwrite: true,
                    ..Default::default()
                },
            )?;

            let commit =
                git::get_latest_commit_for_apps(tmp_dir.path(), &subdir, &[app.to_string()])?
                    .get(app)
                    .ok_or_else(|| anyhow!("Failed to get latest commit for app"))?
                    .clone();
            provider_data.apps.insert(app.to_string(), commit);
        }
        _ => {
            bail!(
                "Unknown app store version in {}: {}",
                store.src,
                app_store_version
            );
        }
    };

    store.provider_data = serde_yaml::to_value(provider_data)?;
    Ok(())
}

pub async fn check_updates_for_store(
    store: AppStore,
    app: Option<String>,
    nirvati_seed: &str,
    user_state: UserState,
    instance_type: InstanceType,
    ctx: &mut ResolverCtx,
) -> Result<Vec<AppUpdateInfo>> {
    let mut updatable_apps = vec![];
    let tmp_dir = TempDir::new("nirvati")?;
    let provider_data: ProviderData = store.provider_data.clone().into();
    let parsed_repo: RepoSrc = store.src.parse()?;
    git::clone(parsed_repo.repo_url, &parsed_repo.branch, tmp_dir.path())?;
    let commit = git::get_commit(tmp_dir.path())?;
    if commit != provider_data.commit {
        let app_store_yml = tmp_dir.path().join("app-store.yml");
        let app_store_yml = load_app_store_yml(&app_store_yml, &store.name.to_string())?;
        match app_store_yml {
            AppStoreYml::AppStoreV1(app_store) => {
                let Some(subdir) = get_subdir(&app_store) else {
                    bail!("No compatible version found for {}", store.src);
                };
                let mut all_store_updatable_apps: Vec<String>;
                let latest_commits = git::get_latest_commit_for_apps(
                    tmp_dir.path(),
                    &subdir,
                    &provider_data
                        .apps
                        .clone()
                        .into_keys()
                        .collect::<Vec<String>>(),
                );
                let Ok(mut latest_commits) = latest_commits else {
                    bail!("Failed to get latest commits for apps in {}", store.src);
                };
                latest_commits.retain(|app_id, commit| {
                    provider_data.apps.contains_key(app_id)
                        && provider_data.apps.get(app_id).unwrap() != commit
                });
                all_store_updatable_apps = latest_commits.into_keys().collect();
                let subdir_path = tmp_dir.path().join(subdir);
                all_store_updatable_apps.retain(|v| subdir_path.join(v).exists());
                for app_id in all_store_updatable_apps {
                    if app.as_ref().is_some_and(|app| app != &app_id) {
                        continue;
                    }
                    let metadata = load_app(
                        &subdir_path,
                        &app_id,
                        user_state.clone(),
                        nirvati_seed,
                        instance_type,
                        None,
                        ctx,
                    )
                    .await?
                    .into_metadata();
                    updatable_apps.push(AppUpdateInfo {
                        id: metadata.id,
                        new_version: metadata.display_version,
                        release_notes: metadata.release_notes,
                    })
                }
            }
        }
    }
    Ok(updatable_apps)
}
