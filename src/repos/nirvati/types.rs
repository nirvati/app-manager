use std::collections::BTreeMap;

use serde::{Deserialize, Serialize};

use crate::parser::app_yml::types::MultiLanguageItem;

pub enum AppStoreYml {
    AppStoreV1(AppStoreV1),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AppStoreV1 {
    pub store_version: u8,

    pub name: MultiLanguageItem,
    pub tagline: MultiLanguageItem,
    pub description: MultiLanguageItem,
    pub icon: String,
    // Developer name -> their website
    pub developers: BTreeMap<String, String>,
    pub license: String,

    pub content: BTreeMap<String, String>,
}
