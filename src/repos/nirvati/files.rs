use std::fs::File;
use std::path::Path;

use anyhow::bail;

use super::types::AppStoreYml;

pub fn load_app_store_yml(file: &Path, store_src: &str) -> anyhow::Result<AppStoreYml> {
    let app_store_yml = File::open(file);
    let Ok(app_store_yml) = app_store_yml else {
        bail!("No app-store.yml found in {}", store_src);
    };
    let app_store = serde_yaml::from_reader::<File, serde_yaml::Value>(app_store_yml);
    let Ok(app_store) = app_store else {
        bail!("Failed to load app-store.yml in {}", store_src);
    };
    let app_store_version = app_store.get("store_version");
    if app_store_version.is_none() || !app_store_version.unwrap().is_u64() {
        bail!("App store version not defined.");
    }
    let app_store_version = app_store_version.unwrap().as_u64().unwrap();
    match app_store_version {
        1 => Ok(AppStoreYml::AppStoreV1(serde_yaml::from_value(app_store)?)),
        _ => bail!("App store version not supported."),
    }
}
