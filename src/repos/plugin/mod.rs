use crate::generator::internal::types::PluginType;
use crate::manage::files::read_plugins_json;
use crate::plugins::api::{DownloadAppRequest, DownloadAppsRequest, UserState};
use crate::repos::types::{AppStore, AppUpdateInfo};
use anyhow::anyhow;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::path::Path;
use tempdir::TempDir;
use tonic::Request;

#[derive(Serialize, Deserialize)]
struct ProviderData {
    pub plugin: String,
    pub state: Option<serde_json::Value>,
}

async fn get_plugin_endpoint(apps_root: &Path, plugin: &str) -> anyhow::Result<String> {
    let plugins_json = read_plugins_json(apps_root).await?;
    let endpoint = plugins_json
        .get(plugin)
        .ok_or_else(|| anyhow!("Plugin not found"))?;
    if endpoint.r#type != PluginType::Source {
        return Err(anyhow!("Plugin is not a source plugin"));
    }
    Ok(endpoint.endpoint.clone())
}

pub async fn download_app(
    apps_root: &Path,
    app: &str,
    store: &mut AppStore,
    user_state: UserState,
) -> anyhow::Result<()> {
    let provider_data: ProviderData = serde_yaml::from_value(store.provider_data.clone())?;
    let plugin = provider_data.plugin;
    let state = provider_data.state;
    let encoded_state = serde_json::to_string(&state.unwrap_or(serde_json::Value::Null))?;
    // First, look up the plugin endpoint
    let endpoint = get_plugin_endpoint(apps_root, &plugin).await?;
    let mut client =
        crate::plugins::api::source_plugin_client::SourcePluginClient::connect(endpoint).await?;
    let app_data = client
        .download_app(Request::new(DownloadAppRequest {
            source: store.src.clone(),
            app_id: app.to_string(),
            state: encoded_state,
            user_state: user_state.clone().into(),
        }))
        .await?
        .into_inner();
    // Now, extract the app data
    let app_dir = apps_root.join(app);
    crate::utils::tar::safe_decompress_dir(app_data.app_directory, &app_dir).await?;
    let parsed_state = serde_json::from_str(&app_data.state)?;
    store.provider_data = serde_yaml::to_value(ProviderData {
        plugin,
        state: Some(parsed_state),
    })?;
    Ok(())
}

pub async fn download_apps_for_store(
    apps_root: &Path,
    store: &mut AppStore,
    skip_apps: &[String],
    user_state: UserState,
) -> anyhow::Result<Vec<String>> {
    let provider_data: ProviderData = serde_yaml::from_value(store.provider_data.clone())?;
    let plugin = provider_data.plugin;
    let state = provider_data.state;
    let encoded_state = serde_json::to_string(&state.unwrap_or(serde_json::Value::Null))?;
    // First, look up the plugin endpoint
    let endpoint = get_plugin_endpoint(apps_root, &plugin).await?;
    let mut client =
        crate::plugins::api::source_plugin_client::SourcePluginClient::connect(endpoint).await?;
    let app_data = client
        .download_all_apps(Request::new(DownloadAppsRequest {
            source: store.src.clone(),
            skip_apps: skip_apps.to_vec(),
            state: encoded_state,
            user_state: user_state.clone().into(),
        }))
        .await?
        .into_inner();
    // Now, extract the app data to a temporary directory
    let mut downloaded_apps = Vec::new();
    let temp_dir = TempDir::new("downloaded_apps")?;
    crate::utils::tar::safe_decompress_dir(app_data.apps_directory, temp_dir.path()).await?;
    for entry in temp_dir.path().read_dir()? {
        let entry = entry?;
        let app = entry.file_name().to_string_lossy().to_string();
        if skip_apps.contains(&app) {
            tracing::warn!(
                "Store plugin tried to download app that was skipped: {}",
                app
            );
            continue;
        }
        let app_dir = apps_root.join(&app);
        std::fs::rename(entry.path(), &app_dir)?;
        downloaded_apps.push(app);
    }
    let parsed_state = serde_json::from_str(&app_data.state)?;
    store.provider_data = serde_yaml::to_value(ProviderData {
        plugin,
        state: Some(parsed_state),
    })?;
    Ok(downloaded_apps)
}

pub async fn check_updates_for_store(
    apps_root: &Path,
    store: AppStore,
    app: Option<String>,
    user_state: UserState,
) -> anyhow::Result<Vec<AppUpdateInfo>> {
    let provider_data: ProviderData = serde_yaml::from_value(store.provider_data.clone())?;
    let plugin = provider_data.plugin;
    let state = provider_data.state;
    let encoded_state = serde_json::to_string(&state.unwrap_or(serde_json::Value::Null))?;
    let endpoint = get_plugin_endpoint(apps_root, &plugin).await?;
    let mut client =
        crate::plugins::api::source_plugin_client::SourcePluginClient::connect(endpoint).await?;
    if let Some(app) = app {
        let resp = client
            .get_update(Request::new(crate::plugins::api::GetUpdateRequest {
                source: store.src,
                app_id: app.clone(),
                state: encoded_state,
                user_state: user_state.into(),
            }))
            .await?
            .into_inner();
        let mut updates = Vec::new();
        if let Some(update) = resp.update {
            let parsed_release_notes: BTreeMap<String, BTreeMap<String, String>> =
                serde_json::from_str(&update.release_notes)?;
            updates.push(AppUpdateInfo {
                id: app,
                new_version: update.new_version,
                release_notes: parsed_release_notes,
            });
        }
        Ok(updates)
    } else {
        let resp = client
            .get_all_updates(Request::new(crate::plugins::api::GetAllUpdatesRequest {
                source: store.src,
                state: encoded_state,
                user_state: user_state.into(),
            }))
            .await?
            .into_inner();
        let mut updates = Vec::new();
        for (app, update) in resp.updates {
            let parsed_release_notes: BTreeMap<String, BTreeMap<String, String>> =
                serde_json::from_str(&update.release_notes)?;
            updates.push(AppUpdateInfo {
                id: app,
                new_version: update.new_version,
                release_notes: parsed_release_notes,
            });
        }
        Ok(updates)
    }
}
