use std::collections::BTreeMap;

use crate::utils::MultiLanguageItem;
use anyhow::anyhow;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct AppStore {
    pub id: uuid::Uuid,
    pub name: MultiLanguageItem,
    pub tagline: MultiLanguageItem,
    pub description: MultiLanguageItem,
    pub icon: String,
    // Developer name -> their website
    pub developers: BTreeMap<String, String>,
    pub license: String,
    #[serde(default)]
    pub r#type: StoreType,
    pub src: String,
    pub apps: Vec<String>,

    // Data specific to the app store type
    pub provider_data: serde_yaml::Value,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AppUpdateInfo {
    pub id: String,
    pub new_version: String,
    pub release_notes: BTreeMap<String, BTreeMap<String, String>>,
}

pub type StoresYml = Vec<AppStore>;

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub enum StoreType {
    #[default]
    Nirvati,
    Plugin,
}

impl TryFrom<i32> for StoreType {
    type Error = anyhow::Error;

    fn try_from(value: i32) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(StoreType::Nirvati),
            -1 => Ok(StoreType::Plugin),
            _ => Err(anyhow!("Invalid data!")),
        }
    }
}
