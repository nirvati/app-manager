use anyhow::{anyhow, Result};
use flate2::write::GzEncoder;
use std::path::Path;
use tar::Header;

pub async fn compress_dir(dir: &Path) -> Result<Vec<u8>> {
    let output = Vec::new();
    let gz_encoder = GzEncoder::new(output, flate2::Compression::default());
    let mut tar = tar::Builder::new(gz_encoder);
    for entry in dir.read_dir()? {
        let entry = entry?;
        let path = entry.path();
        let mut header = Header::new_gnu();
        header.set_size(entry.metadata()?.len());
        header.set_cksum();
        // Set path, but strip the dir prefix
        header.set_path(path.strip_prefix(dir)?)?;
        let file = std::fs::File::open(&path)?;
        tar.append(&header, file)?;
    }
    tar.finish()?;
    Ok(tar.into_inner()?.finish()?)
}

// Decompresses a gz-encoded tarball into a directory, preventing any path traversal attacks
pub async fn safe_decompress_dir(archive: Vec<u8>, target: &Path) -> Result<()> {
    let mut tar = tar::Archive::new(flate2::read::GzDecoder::new(&archive[..]));
    for entry in tar.entries()? {
        let mut entry = entry?;
        let path = entry.path()?;
        let path = target.join(path);
        if !path.starts_with(target) {
            return Err(anyhow!("Path traversal attack detected"));
        }
        // Also check entry.link_name()
        if let Some(link_name) = entry.link_name()? {
            let link_path = target.join(link_name);
            if !link_path.starts_with(target) {
                return Err(anyhow!("Path traversal attack detected"));
            }
        }
        entry.unpack(path)?;
    }
    Ok(())
}
