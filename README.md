# Nirvati App Manager

This is the Nirvati App Manager, which is used to download, update, install, and remove Nirvati apps.

## How it works

![Nirvati App Manager Architecture](./architecture.png)

Nirvati's app manager takes apps in various formats and from various registries and first converts them into its own internal representation.
This format is then further processed and will currently be converted into Kubernetes resources that then get saved as a Helm chart, which we
then upload to a local ChartMuseum instance.

This architecture simplifies adding new app sources and formats, as well as allows us to easily add new output formats, because all security
validation and processing is done on the internal representation.
