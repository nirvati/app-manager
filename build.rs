pub fn main() {
    tonic_build::configure()
        .protoc_arg("--experimental_allow_proto3_optional")
        .compile(
            &[
                "protos/api.proto",
                "protos/plugin.proto",
                "protos/public-api.proto",
            ],
            &["protos"],
        )
        .unwrap();
}
